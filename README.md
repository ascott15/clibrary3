#CLibrary 3#
A re-write of the CLibrary progaram.  The CLibrary program is for a small library.

#Architecture#
1. CLibrary
1. Glassfish / tomEE / Possibly Tomcat with extensions
1. PostgreSQL x.x
1. Ubuntu 16.04

TBC

#Projects#
1. clibrary-db
1. clibrary-service
1. cilbrary-web

TBC

#Requirements#
1. A librarian has requested the addition of resource ids & category names to the Overdue Loans Report. This is to aid in the finding of the resource on the shelves.