/**
 * 
 */
package org.clibrary.db.sqlbuilder;

import org.clibrary.CLibraryException;
import org.clibrary.db.hibernate.Borrower;
import org.clibrary.db.sqlbuilder.BorrowerHqlBuilder;
import org.clibrary.test.utils.TestUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author ascott
 *
 */
public class BorrowerHqlBuilderTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test when Borrower is null.
	 */
	@Test
	public void testBuildSql_null() {

		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Assert.assertEquals((String) null, borrowerSqlBuilder.buildSql(null));
		Assert.assertNull(borrowerSqlBuilder.getParamList());
	}

	/**
	 * Test when Borrower is empty.
	 */
	@Test
	public void testBuildSql_empty() {

		String expectedSql = "select b from Borrower b\norder by b.borrowerid desc";
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(0, borrowerSqlBuilder.getParamList().size());
	}
	
	/**
	 * Test when Borrower only contains the borrower ID.
	 */
	@Test
	public void testBuildSql_borrowerId() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/borrowerid.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setBorrowerid("a");
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(1, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getBorrowerid(), borrowerSqlBuilder.getParamList().get(0));
	}

	/**
	 * Test when Borrower only contains the firstname.
	 */
	@Test
	public void testBuildSql_firstname() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/firstname.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setFirstname("a");
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(1, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getFirstname(), borrowerSqlBuilder.getParamList().get(0));
	}
	
	/**
	 * Test when Borrower only contains the lastname.
	 */
	@Test
	public void testBuildSql_lastname() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/lastname.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setLastname("a");
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(1, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getLastname(), borrowerSqlBuilder.getParamList().get(0));
	}
	
	/**
	 * Test when Borrower only contains address.
	 */
	@Test
	public void testBuildSql_address() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/address.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setAddress("c");
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(1, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getAddress(), borrowerSqlBuilder.getParamList().get(0));
	}
	
	/**
	 * Test when Borrower only contains phone.
	 */
	@Test
	public void testBuildSql_phone() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/phone.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setPhone("d");
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(1, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getPhone(), borrowerSqlBuilder.getParamList().get(0));
	}
	
	/**
	 * Test when Borrower only contains mobile.
	 */
	@Test
	public void testBuildSql_mobile() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/mobile.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setMobile("e");
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(1, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getMobile(), borrowerSqlBuilder.getParamList().get(0));
	}
	
	/**
	 * Test when Borrower only contains email.
	 */
	@Test
	public void testBuildSql_email() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/email.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setEmail("f");
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(1, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getEmail(), borrowerSqlBuilder.getParamList().get(0));
	}
	
	/**
	 * Test when Borrower only contains comment.
	 */
	@Test
	public void testBuildSql_comment() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/comment.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setComment("g");
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(1, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getComment(), borrowerSqlBuilder.getParamList().get(0));
	}
	
	/**
	 * Test when Borrower contains all borrower attributes.
	 */
	@Test
	public void testBuildSql_all() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/all.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setBorrowerid("1");
		borrower.setFirstname("a");
		borrower.setLastname("b");
		borrower.setAddress("c");
		borrower.setPhone("d");
		borrower.setMobile("e");
		borrower.setEmail("f");
		borrower.setComment("g");
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(8, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals(borrower.getBorrowerid(), borrowerSqlBuilder.getParamList().get(0));
		Assert.assertEquals(borrower.getFirstname(), borrowerSqlBuilder.getParamList().get(1));
		Assert.assertEquals(borrower.getLastname(), borrowerSqlBuilder.getParamList().get(2));
		Assert.assertEquals(borrower.getAddress(), borrowerSqlBuilder.getParamList().get(3));
		Assert.assertEquals(borrower.getPhone(), borrowerSqlBuilder.getParamList().get(4));
		Assert.assertEquals(borrower.getMobile(), borrowerSqlBuilder.getParamList().get(5));
		Assert.assertEquals(borrower.getEmail(), borrowerSqlBuilder.getParamList().get(6));
		Assert.assertEquals(borrower.getComment(), borrowerSqlBuilder.getParamList().get(7));
	}
	
	/**
	 * Test when Borrower contains all borrower attributes with a less than borrower ID.
	 */
	@Test
	public void testBuildSql_all_lessThan() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/all.lessthan.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		borrowerSqlBuilder.setOrder("desc");
		Borrower borrower = new Borrower();
		borrower.setFirstname("a");
		borrower.setLastname("b");
		borrower.setAddress("c");
		borrower.setPhone("d");
		borrower.setMobile("e");
		borrower.setEmail("f");
		borrower.setComment("g");
		borrowerSqlBuilder.setLessThanBorrowerId("1");
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(8, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals("1", borrowerSqlBuilder.getParamList().get(0));
		Assert.assertEquals(borrower.getFirstname(), borrowerSqlBuilder.getParamList().get(1));
		Assert.assertEquals(borrower.getLastname(), borrowerSqlBuilder.getParamList().get(2));
		Assert.assertEquals(borrower.getAddress(), borrowerSqlBuilder.getParamList().get(3));
		Assert.assertEquals(borrower.getPhone(), borrowerSqlBuilder.getParamList().get(4));
		Assert.assertEquals(borrower.getMobile(), borrowerSqlBuilder.getParamList().get(5));
		Assert.assertEquals(borrower.getEmail(), borrowerSqlBuilder.getParamList().get(6));
		Assert.assertEquals(borrower.getComment(), borrowerSqlBuilder.getParamList().get(7));
	}
	
	/**
	 * Test when Borrower contains all borrower attributes with a largerf than borrower ID.
	 */
	@Test
	public void testBuildSql_all_largerThan() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/all.largerthan.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setFirstname("a");
		borrower.setLastname("b");
		borrower.setAddress("c");
		borrower.setPhone("d");
		borrower.setMobile("e");
		borrower.setEmail("f");
		borrower.setComment("g");
		borrowerSqlBuilder.setLargerThanBorrowerId("1", false);
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(8, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals("1", borrowerSqlBuilder.getParamList().get(0));
		Assert.assertEquals(borrower.getFirstname(), borrowerSqlBuilder.getParamList().get(1));
		Assert.assertEquals(borrower.getLastname(), borrowerSqlBuilder.getParamList().get(2));
		Assert.assertEquals(borrower.getAddress(), borrowerSqlBuilder.getParamList().get(3));
		Assert.assertEquals(borrower.getPhone(), borrowerSqlBuilder.getParamList().get(4));
		Assert.assertEquals(borrower.getMobile(), borrowerSqlBuilder.getParamList().get(5));
		Assert.assertEquals(borrower.getEmail(), borrowerSqlBuilder.getParamList().get(6));
		Assert.assertEquals(borrower.getComment(), borrowerSqlBuilder.getParamList().get(7));
	}
	
	/**
	 * Test when Borrower contains all borrower attributes with a larger or equal to than borrower ID.
	 */
	@Test
	public void testBuildSql_all_largerThanEqualTo() throws Exception {

		String expectedSql = TestUtils
				.readResource("org/clibrary/db/sqlbuilder/borrowersqlbuildertest/all.largerthanequalto.hql");
		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setFirstname("a");
		borrower.setLastname("b");
		borrower.setAddress("c");
		borrower.setPhone("d");
		borrower.setMobile("e");
		borrower.setEmail("f");
		borrower.setComment("g");
		borrowerSqlBuilder.setLargerThanBorrowerId("1", true);
		  
		String actualSql = borrowerSqlBuilder.buildSql(borrower);
		expectedSql = TestUtils.prepareToCompare(expectedSql);
		actualSql = TestUtils.prepareToCompare(actualSql);
		Assert.assertEquals(expectedSql, actualSql);
		Assert.assertEquals(8, borrowerSqlBuilder.getParamList().size());
		Assert.assertEquals("1", borrowerSqlBuilder.getParamList().get(0));
		Assert.assertEquals(borrower.getFirstname(), borrowerSqlBuilder.getParamList().get(1));
		Assert.assertEquals(borrower.getLastname(), borrowerSqlBuilder.getParamList().get(2));
		Assert.assertEquals(borrower.getAddress(), borrowerSqlBuilder.getParamList().get(3));
		Assert.assertEquals(borrower.getPhone(), borrowerSqlBuilder.getParamList().get(4));
		Assert.assertEquals(borrower.getMobile(), borrowerSqlBuilder.getParamList().get(5));
		Assert.assertEquals(borrower.getEmail(), borrowerSqlBuilder.getParamList().get(6));
		Assert.assertEquals(borrower.getComment(), borrowerSqlBuilder.getParamList().get(7));
	}

	/**
	 * Test when Borrower only contains the firstname.
	 */
	@Test
	public void testBuildSql_error_lessThanAndLargeThan() throws Exception {

		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrowerSqlBuilder.setLessThanBorrowerId("z");
		borrowerSqlBuilder.setLargerThanBorrowerId("a", true);

		try {
			borrowerSqlBuilder.buildSql(borrower);
			Assert.fail("Expected a CLibraryException.");
		} catch (CLibraryException e) {
			Assert.assertEquals("Both lessThanBorrowerId and largerThanBorrowerId cannot be used.", e.getMessage());
		}
	}
	
	/**
	 * Test when Borrower only contains the firstname.
	 */
	@Test
	public void testBuildSql_error_borrowerIdAndlessThan() throws Exception {

		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setBorrowerid("1");
		borrowerSqlBuilder.setLessThanBorrowerId("z");

		try {
			borrowerSqlBuilder.buildSql(borrower);
			Assert.fail("Expected a CLibraryException.");
		} catch (CLibraryException e) {
			Assert.assertEquals("Both Borrower.borrowerId and lessThanBorrowerId cannot be used.", e.getMessage());
		}
	}
	
	/**
	 * Test when Borrower only contains the firstname.
	 */
	@Test
	public void testBuildSql_error_borrowerIdAndLargeThan() throws Exception {

		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
		Borrower borrower = new Borrower();
		borrower.setBorrowerid("1");
		borrowerSqlBuilder.setLargerThanBorrowerId("a", true);

		try {
			borrowerSqlBuilder.buildSql(borrower);
			Assert.fail("Expected a CLibraryException.");
		} catch (CLibraryException e) {
			Assert.assertEquals("Both Borrower.borrowerId and largerThanBorrowerId cannot be used.", e.getMessage());
		}
	}
	
//	/**
//	 * Test when Borrower only contains the firstname.
//	 */
//	@Test
//	public void testBuildSql_error_numRecordsIsZero() throws Exception {
//
//		BorrowerHqlBuilder borrowerSqlBuilder = new BorrowerHqlBuilder();
//		Borrower borrower = new Borrower();
//
//		try {
//			borrowerSqlBuilder.buildSql(borrower, 0);
//			Assert.fail("Expected a CLibraryException.");
//		} catch (CLibraryException e) {
//			Assert.assertEquals("The numRecords parameter must be larger than zero.", e.getMessage());
//		}
//	}
}
