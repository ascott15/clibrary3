package org.clibrary.db.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.clibrary.db.sql.springconfig.CLibraryDbSpringTestConfig;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CLibraryDbSpringTestConfig.class })
public class ResourceDaoImplTest {

	@PersistenceContext
	EntityManager entityManager;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testSprint() {
		
	}

	@Test
	public void testFindAll() {
		
		ResourceDao resourceDao = new ResourceDaoImpl(entityManager);

		Assert.assertEquals(0, resourceDao.findAll().size());
	}
}
