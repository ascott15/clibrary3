package org.clibrary.db.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.clibrary.db.dao.CategoryDao;
import org.clibrary.db.h2.H2Helper;
import org.clibrary.db.sql.springconfig.CLibraryDbSpringTestConfig;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CLibraryDbSpringTestConfig.class })
public class CategoryDaoImplTest {
	
	@Autowired
	H2Helper h2Helper;
	
	@PersistenceContext
	EntityManager entityManager;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testSprint() {
		
	}

	@Test
	public void testFindAll() throws Exception {
		
		CategoryDao categoryDao = new CategoryDaoImpl(entityManager);

		Assert.assertEquals(12, categoryDao.findAll().size());
	}
}
