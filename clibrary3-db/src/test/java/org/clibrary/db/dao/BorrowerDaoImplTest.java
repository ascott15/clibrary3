package org.clibrary.db.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.clibrary.db.h2.H2Helper;
import org.clibrary.db.hibernate.Borrower;
import org.clibrary.db.sql.springconfig.CLibraryDbSpringTestConfig;
import org.clibrary.test.utils.TestUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CLibraryDbSpringTestConfig.class })
public class BorrowerDaoImplTest {

	@Inject
	H2Helper h2Helper;

	@PersistenceContext
	EntityManager entityManager;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

		h2Helper.reset();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInsertGet() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		Assert.assertEquals(0, borrowerDao.findAll().size());

		Borrower insertBorrower1 = new Borrower();
		insertBorrower1.setFirstname("Bob");
		insertBorrower1.setLastname("Smith");
		insertBorrower1.setAddress("12 Somewhere St");
		insertBorrower1.setPhone("07 1111 2222");
		insertBorrower1.setMobile("0741 333 444");
		insertBorrower1.setEmail("Bob.Smith@SomeEmail.com.au");
		insertBorrower1.setComment("Bob can only come to the library once a month, the first of every month.");
		entityManager.persist(insertBorrower1);

		Borrower insertBorrower2 = new Borrower();
		insertBorrower2.setFirstname("Jane");
		insertBorrower2.setLastname("Smith");
		insertBorrower2.setAddress("12 Somewhere St");
		insertBorrower2.setPhone("07 1111 2222");
		insertBorrower2.setMobile("0741 555 666");
		insertBorrower2.setEmail("Jane.Smith@AnotherEmail.com.au");
		insertBorrower2.setComment("Jane can only come to the library once a month, the first of every month.");
		entityManager.persist(insertBorrower2);

		Borrower insertBorrower3 = new Borrower();
		insertBorrower3.setFirstname("Frank");
		insertBorrower3.setLastname("Smith");
		insertBorrower3.setAddress("25 Elsewhere St");
		insertBorrower3.setPhone("07 3333 4444");
		insertBorrower3.setMobile("0741 777 888");
		insertBorrower3.setEmail("Frank.Smith@FastEmail.com.au");
		// insertBorrower3.setComment();
		entityManager.persist(insertBorrower3);

		Borrower insertBorrower4 = new Borrower();
		insertBorrower4.setFirstname("Jack");
		insertBorrower4.setLastname("Jones");
		insertBorrower4.setAddress("25 Elsewhere St");
		// insertBorrower4.setPhone();
		// insertBorrower4.setMobile();
		// insertBorrower4.setEmail();
		// insertBorrower3.setComment();
		entityManager.persist(insertBorrower4);

		Borrower getBorrower1 = entityManager.find(Borrower.class, "B00001");
		Assert.assertEquals("B00001", getBorrower1.getBorrowerid());
		Assert.assertEquals(getBorrower1.getFirstname(), insertBorrower1.getFirstname());
		Assert.assertEquals(getBorrower1.getLastname(), insertBorrower1.getLastname());
		Assert.assertEquals(getBorrower1.getAddress(), insertBorrower1.getAddress());
		Assert.assertEquals(getBorrower1.getPhone(), insertBorrower1.getPhone());
		Assert.assertEquals(getBorrower1.getMobile(), insertBorrower1.getMobile());
		Assert.assertEquals(getBorrower1.getEmail(), insertBorrower1.getEmail());
		Assert.assertEquals(getBorrower1.getComment(), insertBorrower1.getComment());

		Borrower getBorrower2 = entityManager.find(Borrower.class, "B00002");
		Assert.assertEquals("B00002", getBorrower2.getBorrowerid());
		Assert.assertEquals(getBorrower2.getFirstname(), insertBorrower2.getFirstname());
		Assert.assertEquals(getBorrower2.getLastname(), insertBorrower2.getLastname());
		Assert.assertEquals(getBorrower2.getAddress(), insertBorrower2.getAddress());
		Assert.assertEquals(getBorrower2.getPhone(), insertBorrower2.getPhone());
		Assert.assertEquals(getBorrower2.getMobile(), insertBorrower2.getMobile());
		Assert.assertEquals(getBorrower2.getEmail(), insertBorrower2.getEmail());
		Assert.assertEquals(getBorrower2.getComment(), insertBorrower2.getComment());

		Borrower getBorrower3 = entityManager.find(Borrower.class, "B00003");
		Assert.assertEquals("B00003", getBorrower3.getBorrowerid());
		Assert.assertEquals(getBorrower3.getFirstname(), insertBorrower3.getFirstname());
		Assert.assertEquals(getBorrower3.getLastname(), insertBorrower3.getLastname());
		Assert.assertEquals(getBorrower3.getAddress(), insertBorrower3.getAddress());
		Assert.assertEquals(getBorrower3.getPhone(), insertBorrower3.getPhone());
		Assert.assertEquals(getBorrower3.getMobile(), insertBorrower3.getMobile());
		Assert.assertEquals(getBorrower3.getEmail(), insertBorrower3.getEmail());
		Assert.assertEquals(getBorrower3.getComment(), insertBorrower3.getComment());

		Borrower getBorrower4 = entityManager.find(Borrower.class, "B00004");
		Assert.assertEquals("B00004", getBorrower4.getBorrowerid());
		Assert.assertEquals(getBorrower4.getFirstname(), insertBorrower4.getFirstname());
		Assert.assertEquals(getBorrower4.getLastname(), insertBorrower4.getLastname());
		Assert.assertEquals(getBorrower4.getAddress(), insertBorrower4.getAddress());
		Assert.assertEquals(getBorrower4.getPhone(), insertBorrower4.getPhone());
		Assert.assertEquals(getBorrower4.getMobile(), insertBorrower4.getMobile());
		Assert.assertEquals(getBorrower4.getEmail(), insertBorrower4.getEmail());
		Assert.assertEquals(getBorrower4.getComment(), insertBorrower4.getComment());

		Assert.assertEquals(4, borrowerDao.findAll().size());
	}

	/**
	 * Find with no parameters.
	 * 
	 * @throws Exception
	 */
	@Test
	public void find() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.find(borrower, 2);
		Assert.assertEquals(2, borrowerList.size());
		Assert.assertEquals("B00001", borrowerList.get(0).getBorrowerid());
		Assert.assertEquals("B00002", borrowerList.get(1).getBorrowerid());
	}

	/**
	 * Find larger than with 0 results.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.findForward(borrower, "B00004", false, 2);
		Assert.assertEquals(0, borrowerList.size());
	}

	/**
	 * Find larger than with 1 result.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward_withLastId_withLargerEqual() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.findForward(borrower, "B00004", true, 2);
		Assert.assertEquals(1, borrowerList.size());
		Assert.assertEquals("B00004", borrowerList.get(0).getBorrowerid());
	}

	/**
	 * Find larger than with 2 results with no start ID.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward_emptyBorrowerId() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.findForward(borrower, "", false, 2);
		Assert.assertEquals(2, borrowerList.size());
		Assert.assertEquals("B00001", borrowerList.get(0).getBorrowerid());
		Assert.assertEquals("B00002", borrowerList.get(1).getBorrowerid());
	}

	/**
	 * Find larger than with 2 results with start ID.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward_twoResults() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.findForward(borrower, "B00001", false, 2);
		Assert.assertEquals(2, borrowerList.size());
		Assert.assertEquals("B00002", borrowerList.get(0).getBorrowerid());
		Assert.assertEquals("B00003", borrowerList.get(1).getBorrowerid());
	}

	/**
	 * Find larger than with 2 results with start ID and criteria.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward_withCriteria_withPagination() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		borrower.setAddress("12 Somewhere St");
		List<Borrower> borrowerList = borrowerDao.findForward(borrower, "", false, 1);
		Assert.assertEquals(1, borrowerList.size());
		Assert.assertEquals("B00001", borrowerList.get(0).getBorrowerid());

		borrowerList = borrowerDao.findForward(borrower, "B00001", false, 1);
		Assert.assertEquals(1, borrowerList.size());
		Assert.assertEquals("B00002", borrowerList.get(0).getBorrowerid());
	}

	/**
	 * Find less than.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.findBackward(borrower, "B00004", 2);
		Assert.assertEquals(2, borrowerList.size());
		Assert.assertEquals("B00003", borrowerList.get(0).getBorrowerid());
		Assert.assertEquals("B00002", borrowerList.get(1).getBorrowerid());
	}

	/**
	 * Find larger than with 1 result.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward_withLastId() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.findBackward(borrower, "B00002", 2);
		Assert.assertEquals(1, borrowerList.size());
		Assert.assertEquals("B00001", borrowerList.get(0).getBorrowerid());
	}

	/**
	 * Find larger than with 2 results with no start ID.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward_emptyBorrowerId() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.findBackward(borrower, "B00005", 2);
		Assert.assertEquals(2, borrowerList.size());
		Assert.assertEquals("B00004", borrowerList.get(0).getBorrowerid());
		Assert.assertEquals("B00003", borrowerList.get(1).getBorrowerid());
	}

	/**
	 * Find larger than with 2 results with start ID.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward_twoResults() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		List<Borrower> borrowerList = borrowerDao.findBackward(borrower, "B00004", 2);
		Assert.assertEquals(2, borrowerList.size());
		Assert.assertEquals("B00003", borrowerList.get(0).getBorrowerid());
		Assert.assertEquals("B00002", borrowerList.get(1).getBorrowerid());
	}

	/**
	 * Find larger than with 2 results with start ID and criteria.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward_withCriteria_withPagination() throws Exception {

		BorrowerDao borrowerDao = new BorrowerDaoImpl(entityManager);

		testInsertGet();

		Borrower borrower = new Borrower();
		borrower.setAddress("12 Somewhere St");
		List<Borrower> borrowerList = borrowerDao.findBackward(borrower, "B00003", 1);
		Assert.assertEquals(1, borrowerList.size());
		Assert.assertEquals("B00002", borrowerList.get(0).getBorrowerid());

		borrowerList = borrowerDao.findBackward(borrower, "B00002", 1);
		Assert.assertEquals(1, borrowerList.size());
		Assert.assertEquals("B00001", borrowerList.get(0).getBorrowerid());
	}

	/**
	 * Ensure that all of the expected hql queries can be executed.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testExpectedSql() throws Exception {

		String resourceDir = "org/clibrary/db/sqlbuilder/borrowersqlbuildertest/";
		String[] resources = { "address.hql", "all.largerthanequalto.hql", "all.largerthan.hql", "all.lessthan.hql",
				"all.hql", "borrowerid.hql", "comment.hql", "email.hql", "firstname.hql", "lastname.hql", "mobile.hql",
				"phone.hql" };

		for (String resource : resources) {
			String queryStr = TestUtils.readResource(resourceDir + resource);
			Query query = entityManager.createQuery(queryStr, Borrower.class);
			for (Parameter<?> param : query.getParameters()) {
				query.setParameter(param.getPosition(), Integer.toString(param.getPosition()));
			}
			List<Borrower> borrowerList = query.getResultList();
		}
	}
}
