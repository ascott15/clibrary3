package org.clibrary.db.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.clibrary.db.h2.H2Helper;
import org.clibrary.db.hibernate.Borrower;
import org.clibrary.db.hibernate.Resourcetype;
import org.clibrary.db.sql.springconfig.CLibraryDbSpringTestConfig;
import org.clibrary.test.utils.TestUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CLibraryDbSpringTestConfig.class })
public class ResourceTypeDaoImplTest {

	@Inject
	H2Helper h2Helper;

	@PersistenceContext
	EntityManager entityManager;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testInsertGet() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		Assert.assertEquals(6, resourceTypeDao.findAll().size());

		Resourcetype resourcetype1 = new Resourcetype();
		resourcetype1.setResourcetype("book2");
		entityManager.persist(resourcetype1);

		Resourcetype resourcetype2 = new Resourcetype();
		resourcetype2.setResourcetype("video2");
		entityManager.persist(resourcetype2);

		Resourcetype resourcetype3 = new Resourcetype();
		resourcetype3.setResourcetype("DVD2");
		entityManager.persist(resourcetype3);

		Resourcetype resourcetype4 = new Resourcetype();
		resourcetype4.setResourcetype("bluRay2");
		entityManager.persist(resourcetype4);

		Resourcetype getResourcetype1 = entityManager.find(Resourcetype.class, "book2");
		Assert.assertEquals("book2", getResourcetype1.getResourcetype());

		Resourcetype getResourcetype2 = entityManager.find(Resourcetype.class, "video2");
		Assert.assertEquals("video2", getResourcetype2.getResourcetype());

		Resourcetype getResourcetype3 = entityManager.find(Resourcetype.class, "DVD2");
		Assert.assertEquals("DVD2", getResourcetype3.getResourcetype());

		Resourcetype getResourcetype4 = entityManager.find(Resourcetype.class, "bluRay2");
		Assert.assertEquals("bluRay2", getResourcetype4.getResourcetype());

		Assert.assertEquals(10, resourceTypeDao.findAll().size());
	}
	
	// The H2 and Postgress databases sort differently.  H2 is case sensitive but Postgres is not.
	// CD
	// CD-ROM
	// DVD
	// DVD2
	// bluRay2
	// book
	// book2
	// cassette tape
	// video
	// video2

	/**
	 * Find with no parameters.
	 * 
	 * @throws Exception
	 */
	@Test
	public void find() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.find(resourcetype, 10);
		Assert.assertEquals(10, resourcetypeList.size());
		int index = 0;
		Assert.assertEquals("CD", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("CD-ROM", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("DVD", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("DVD2", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("bluRay2", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("book", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("book2", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("cassette tape", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("video", resourcetypeList.get(index++).getResourcetype());
		Assert.assertEquals("video2", resourcetypeList.get(index++).getResourcetype());
	}

	/**
	 * Find larger than with 0 results.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findForward(resourcetype, "video2", false, 2);
		Assert.assertEquals(0, resourcetypeList.size());
	}

	/**
	 * Find larger than with 1 result.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward_withLastId_withLargerEqual() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findForward(resourcetype, "video2", true, 2);
		Assert.assertEquals(1, resourcetypeList.size());
		Assert.assertEquals("video2", resourcetypeList.get(0).getResourcetype());
	}

	/**
	 * Find larger than with 2 results with no start ID.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward_emptyBorrowerId() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findForward(resourcetype, "", false, 2);
		Assert.assertEquals(2, resourcetypeList.size());
		Assert.assertEquals("CD", resourcetypeList.get(0).getResourcetype());
		Assert.assertEquals("CD-ROM", resourcetypeList.get(1).getResourcetype());
	}

	/**
	 * Find larger than with 2 results with start ID.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward_twoResults() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findForward(resourcetype, "bluRay2", false, 2);
		Assert.assertEquals(2, resourcetypeList.size());
		Assert.assertEquals("book", resourcetypeList.get(0).getResourcetype());
		Assert.assertEquals("book2", resourcetypeList.get(1).getResourcetype());
	}

	/**
	 * Find larger than with 2 results with start ID and criteria.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findForward_withCriteria_withPagination() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findForward(resourcetype, "", false, 1);
		Assert.assertEquals(1, resourcetypeList.size());
		Assert.assertEquals("CD", resourcetypeList.get(0).getResourcetype());

		resourcetypeList = resourceTypeDao.findForward(resourcetype, "CD", false, 1);
		Assert.assertEquals(1, resourcetypeList.size());
		Assert.assertEquals("CD-ROM", resourcetypeList.get(0).getResourcetype());
	}

	/**
	 * Find less than.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findBackward(resourcetype, "DVD", 2);
		Assert.assertEquals(2, resourcetypeList.size());
		Assert.assertEquals("CD-ROM", resourcetypeList.get(0).getResourcetype());
		Assert.assertEquals("CD", resourcetypeList.get(1).getResourcetype());
	}

	/**
	 * Find larger than with 1 result.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward_withLastId() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findBackward(resourcetype, "CD-ROM", 2);
		Assert.assertEquals(1, resourcetypeList.size());
		Assert.assertEquals("CD", resourcetypeList.get(0).getResourcetype());
	}

	/**
	 * Find larger than with 2 results with no start ID.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward_emptyBorrowerId() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

//		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findBackward(resourcetype, "zzzzz", 2);
		Assert.assertEquals(2, resourcetypeList.size());
		Assert.assertEquals("video", resourcetypeList.get(0).getResourcetype());
		Assert.assertEquals("cassette tape", resourcetypeList.get(1).getResourcetype());
	}

	/**
	 * Find larger than with 2 results with start ID.
	 * 
	 * @throws Exception
	 */
	@Test
	public void findBackward_twoResults() throws Exception {

		ResourceTypeDao resourceTypeDao = new ResourceTypeDaoImpl(entityManager);

		testInsertGet();

		Resourcetype resourcetype = new Resourcetype();
		List<Resourcetype> resourcetypeList = resourceTypeDao.findBackward(resourcetype, "DVD", 2);
		Assert.assertEquals(2, resourcetypeList.size());
		Assert.assertEquals("CD-ROM", resourcetypeList.get(0).getResourcetype());
		Assert.assertEquals("CD", resourcetypeList.get(1).getResourcetype());
	}

	/**
	 * Ensure that all of the expected hql queries can be executed.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testExpectedSql() throws Exception {

		String resourceDir = "org/clibrary/db/sqlbuilder/borrowersqlbuildertest/";
		String[] resources = { "address.hql", "all.largerthanequalto.hql", "all.largerthan.hql", "all.lessthan.hql",
				"phone.hql" };

		for (String resource : resources) {
			String queryStr = TestUtils.readResource(resourceDir + resource);
			Query query = entityManager.createQuery(queryStr, Borrower.class);
			for (Parameter<?> param : query.getParameters()) {
				query.setParameter(param.getPosition(), Integer.toString(param.getPosition()));
			}
			List<Resourcetype> resourcetypeList = query.getResultList();
		}
	}
}
