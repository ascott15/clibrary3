package org.clibrary.db.h2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

public class H2Helper {

	private final static String SQL_H2_SELECT_ALL_TABLES = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = SCHEMA()";
	// private final static int MAX_COLUMN_SIZE = 100;

	private DataSource dataSource;

	public H2Helper(DataSource dataSource) {

		this.dataSource = dataSource;
	}
	
	public void reset() throws SQLException {

		try (Connection conn = dataSource.getConnection()) {

			Statement stmt = conn.createStatement();
			stmt.execute("drop all objects");
			stmt.execute("runscript from 'classpath:org/clibrary/db/h2/SetupDb.h2.sql'");
			stmt.execute("runscript from 'classpath:org/clibrary/db/h2/SetupDb.CLibrary3Additions.h2.sql'");
		}
	}

	public void dumpAllTables() throws SQLException {

		/*
		 * Create a new connection to ensure that only committed data is
		 * returned, assuming an isolation level of read committed.
		 */
		try (Connection conn = dataSource.getConnection()) {

			Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet resultSet = stmt.executeQuery(SQL_H2_SELECT_ALL_TABLES);

			while (resultSet.next()) {

				dumpTable(resultSet.getString(1));
			}
		}
	}

	public void dumpTable(final String table) throws SQLException {

		System.out.println("\n" + table + ":");
		dumpSql("select * from " + table);
	}

	public void dumpSql(final String sqlStr) throws SQLException {

		/*
		 * Create a new connection to ensure that only committed data is
		 * returned, assuming an isolation level of read committed.
		 */
		try (Connection conn = dataSource.getConnection()) {

			Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet resultSet = stmt.executeQuery(sqlStr);
			dumpResultSet(resultSet);
		}
	}

	public void dumpResultSet(final ResultSet resultSet) throws SQLException {

		/*
		 * Prepare the column details.
		 */
		ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
		int numColumns = resultSetMetaData.getColumnCount();

		ColumnDetails[] columnDetailsArray = new ColumnDetails[numColumns];

		for (int index = 1; index <= numColumns; index++) {

			ColumnDetails columnDetails = new ColumnDetails(index, resultSetMetaData.getColumnLabel(index),
					resultSetMetaData.getColumnDisplaySize(index), resultSetMetaData.getColumnClassName(index));

			columnDetailsArray[index - 1] = columnDetails;
		}

		/*
		 * Set the maximum column size.
		 */
		while (resultSet.next()) {

			for (int index = 1; index <= numColumns; index++) {

				columnDetailsArray[index - 1].recordValueSize(resultSet.getObject(index));
			}
		}

		/*
		 * Display the column heading.
		 */
		for (int index = 1; index <= numColumns; index++) {

			columnDetailsArray[index - 1].displayLabel();
			System.out.print(" ");
		}
		System.out.println();

		for (int index = 1; index <= numColumns; index++) {

			columnDetailsArray[index - 1].displayLabelDivider();
			System.out.print(" ");
		}
		System.out.println();

		/*
		 * Display the columns.
		 */
		resultSet.beforeFirst();
		while (resultSet.next()) {

			for (int index = 1; index <= numColumns; index++) {

				columnDetailsArray[index - 1].displayValue(resultSet.getObject(index));
				System.out.print(" ");
			}
			System.out.println();
		}
	}

	public static class ColumnDetails {

		private int index;
		private String label;
		private int displaySize;
		private String className;
		private String columnFormatStr;

		public ColumnDetails(int index, String label, int displaySize, String className) {

			this.index = index;
			this.label = label;
			this.displaySize = displaySize;
			this.className = className;

			this.displaySize = label.length();
			// if (this.displaySize > MAX_COLUMN_SIZE) {
			//
			// this.displaySize = label.length();
			// }
			//
			// if (this.displaySize < label.length()) {
			//
			// this.displaySize = label.length();
			// }
		}

		private String getColumnFormatStr() {

			if (columnFormatStr == null) {

				columnFormatStr = "%-" + displaySize + "s";
			}

			return columnFormatStr;
		}

		public void displayLabel() {

			System.out.printf(getColumnFormatStr(), label);
		}

		public void displayLabelDivider() {

			for (int counter = 1; counter <= displaySize; counter++) {

				System.out.print("=");
			}
		}

		public void displayValue(final Object object) {

			String valueStr = null;
			if (object == null) {

				valueStr = "";

			} else {

				valueStr = object.toString();
			}

			if (valueStr.length() > displaySize) {

				valueStr = valueStr.substring(0, displaySize);
			}

			System.out.printf(getColumnFormatStr(), valueStr);
		}

		public void recordValueSize(final Object object) {

			if (object == null) {

				return;
			}

			String valueStr = object.toString();
			int valueLen = valueStr.length();
			if (valueLen > displaySize) {

				displaySize = valueLen;
			}
		}
	}
}
