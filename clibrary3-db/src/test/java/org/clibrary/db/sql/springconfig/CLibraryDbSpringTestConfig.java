package org.clibrary.db.sql.springconfig;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;

import org.clibrary.db.h2.H2Helper;
import org.clibrary.db.sql.springconfig.CLibraryDbSpringConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Can use either of the following annotations to define a producer. It appears
 * the EJB 3.1 @Produces annotation is not yet supported.
 * <ul>
 * <li>@Component
 * <li>@Configuration - Uses CGLIB for access. Probably to ensure singleton
 * creation and to allow for circular reference.
 * </ul>
 * Other points:
 * <ul>
 * <li>Can use EJB 3.1 annotations if javax.inject is in the maven depencencies.
 * See http
 * ://static.springsource.org/spring/docs/3.2.x/spring-framework-reference/html
 * /beans.html chapter '5.11.3 Limitations of the standard approach'.
 * <li>Every bean has a default name.
 * </ul>
 * 
 * @author ascott
 * 
 */
@Configuration
@PropertySource("classpath:conf/database.properties")
@EnableTransactionManagement
public class CLibraryDbSpringTestConfig extends CLibraryDbSpringConfig {

	private EmbeddedDatabase db;
	private H2Helper h2Helper;

	public CLibraryDbSpringTestConfig() {

		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		builder.setType(EmbeddedDatabaseType.H2);
		builder.addScript("org/clibrary/db/h2/SetupDb.h2.sql");
		builder.addScript("org/clibrary/db/h2/SetupDb.CLibrary3Additions.h2.sql");

		db = builder.build();
	}

	@Override
	@Bean
	public DataSource getDataSource() {

		return db;
	}

	@Bean(name = "h2Helper")
	public H2Helper getH2Helper() {

		if (h2Helper == null) {

			h2Helper = new H2Helper(getDataSource());
		}

		return h2Helper;
	}

	@PreDestroy
	public void close() {

		db.shutdown();
	}
}
