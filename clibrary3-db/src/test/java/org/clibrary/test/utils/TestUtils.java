package org.clibrary.test.utils;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public class TestUtils {

	/**
	 * Read a resource from the classpath.
	 * 
	 * @param resourceName
	 *            The resource name and path. For example,
	 *            "au/net/ascott/clibrary/db/h2/SetupDB.h2.sql".
	 * @return The contents of the resource.
	 * @throws IOException
	 */
	public static String readResource(final String resourceName) throws IOException {

		try (InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceName)) {
			if (inStream == null) {
				throw new RuntimeException("Could not find resource '" + resourceName + "'.");
			}
			String contents = IOUtils.toString(inStream, "UTF-8");
			return contents;
		}
	}

	/**
	 * Prepare the string for comparison. Trim the string and replace carriage
	 * return and line feed with just line feed.
	 * 
	 * @param str
	 * @return
	 */
	public static String prepareToCompare(final String str) {

		if (str == null) {
			return str;
		}
		String returnStr = str.trim();
		returnStr = returnStr.replace("\r\n", "\n");
		return returnStr;
	}
}
