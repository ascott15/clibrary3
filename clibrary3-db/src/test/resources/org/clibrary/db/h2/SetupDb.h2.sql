--
-- Table structure for table `authors`
-- 


-- --------------------------------------------------------


--
-- Borrowers
--
-- This table is used to identify the borrowers who borrower resources.
--

create table Borrower(
	BorrowerID varchar(6) not null,
	FirstName varchar(25) default null,
	LastName varchar(25) default null,
	Address varchar(255) default null,
	Phone varchar(15) default null,
	Mobile varchar(15) default null,
	Email varchar(255) default null,
	Comment varchar(255) default null,
	primary key (BorrowerID)
);


-- --------------------------------------------------------


--
-- Owner
-- 
-- Not currently used.
--

create table Owner(
	OwnerID decimal(18,0) not null,
	OwnerFirstName varchar(15) not null,
	OwnerLastName varchar(15) not null,
	OwnerPhone varchar(15) not null,
	primary key (OwnerID)
);

-- create generator OwnerIDGen;
-- set generator OwnerIDGen TO 0;
CREATE SEQUENCE OwnerIDGen
   INCREMENT 1
   START 0
   MINVALUE 0;
--ALTER TABLE OwnerIDGen OWNER TO clibrary;


-- --------------------------------------------------------


--
-- Category
--
-- A list of the resource categories.  This field is used for a drop
-- down list.
--

create table Category(
	CategoryName varchar(25) not null,
	primary key ( CategoryName )
);

insert into Category
values ( 'Fiction' );

insert into Category
values ( 'Non-Fiction' );

insert into Category
values ( 'Ellen White' );

insert into Category
values ( 'Evangelism' );

insert into Category
values ( 'Health & Lifestyle' );

insert into Category
values ( 'Spiritual' );

insert into Category
values ( 'Family & Personal' );

insert into Category
values ( 'Biographical' );

insert into Category
values ( 'Special Studies' );

insert into Category
values( 'News' );

insert into Category
values( 'Children' );

insert into Category
values( 'History' );



-- --------------------------------------------------------

--
-- ResourceType
--
-- A list of the types of the valid resources.
--

create table ResourceType(
	ResourceType varchar(25) not null,
	primary key ( ResourceType )
);

insert into ResourceType
values( 'book' );

insert into ResourceType
values( 'cassette tape' );

insert into ResourceType
values( 'CD' );

insert into ResourceType
values( 'CD-ROM' );

insert into ResourceType
values( 'DVD' );

insert into ResourceType
values( 'video' );


-- --------------------------------------------------------


-- Resource
-- Used to describe a resource.

create table Resource(
	ResourceID varchar(6) not null,
	ResourceName varchar(255) not null,
	ISBN varchar(50),
	CategoryName varchar(25) not null,
	ResourceType varchar(25) not null,
	AuthorFirstName varchar(25),
	AuthorMiddleName varchar(25),
	AuthorLastName varchar(25) not null,
	Comment varchar(255),
   	state character(1) NOT NULL DEFAULT 'A',
	primary key (ResourceID),
	foreign key (CategoryName) references Category (CategoryName),
	foreign key (ResourceType) references ResourceType (ResourceType)
);

CREATE INDEX "ResourceStateIndex"
   ON resource (state ASC NULLS LAST);


-- --------------------------------------------------------


--
-- Loan
--
-- Used to identify the loan of a single resource.
--
-- The comment is 255 (borrower comment) + 255 (return comment) + 50 
-- (comment sepeartor).
--

create table Loan(
	LoanID decimal(18,0) not null,
	BorrowerID varchar(6) not null,
	ResourceID varchar(6) not null,
	Borrowed timestamp not null,
	Returned timestamp,
	Comment varchar(600),
	primary key  (LoanID),
	foreign key (BorrowerID) references Borrower (BorrowerID),
	foreign key (ResourceID) references Resource (ResourceID)
);

create index LoanBorrowedIdx on Loan( Borrowed );
create index LoanReturnedIdx on Loan( Returned );

-- create generator LoanIDGen;
-- set generator LoanIDGen TO 0;
CREATE SEQUENCE LoanIDGen
   INCREMENT 1
   START 0
   MINVALUE 0;
--ALTER TABLE LoanIDGen OWNER TO clibrary;


-- --------------------------------------------------------


-- UserPassword
-- 
-- Not currently used.
--
create table UserPassword(
	BorrowerID varchar(6) not null,
	UserPassword varchar(15) not null,
	primary key (BorrowerID),
	foreign key (BorrowerID) references Borrower (BorrowerID)
);

commit;