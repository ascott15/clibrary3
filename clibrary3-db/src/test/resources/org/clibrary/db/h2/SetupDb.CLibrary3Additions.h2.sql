-- The old way of creating Borrower IDs would not work with multiple concurrent users.
-- The new version will use a sequence.
--create sequence BorrowerIdGen increment 1 start 1 minvalue 1;
create sequence borroweridgen increment 1 start 1 minvalue 1;

-- select * from Borrower where firstname is null or lastname is null or address is null;
-- These records must be updated to populate the firstname, lastname, and address, even if it is with 'unknown'.
alter table Borrower alter column firstname set not null;
alter table Borrower alter column lastname set not null;
alter table Borrower alter column address set not null;