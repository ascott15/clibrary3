select b from Borrower b
where borrowerid = ?1
and lower(firstname) like ?2
and lower(lastname) like ?3
and lower(address) like ?4
and phone like ?5
and mobile like ?6
and lower(email) like ?7
and lower(comment) like ?8
order by b.borrowerid