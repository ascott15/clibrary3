package org.clibrary;

public class CLibraryException extends RuntimeException {

	public CLibraryException() {
	}

	public CLibraryException(String message) {
		
		super(message);
	}

	public CLibraryException(String message, Throwable cause) {
		
		super(message, cause);
	}

	protected CLibraryException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CLibraryException(Throwable cause) {
		
		super(cause);
	}
}
