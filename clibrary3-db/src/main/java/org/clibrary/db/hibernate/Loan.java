package org.clibrary.db.hibernate;
// Generated 18/03/2016 5:20:20 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Loan generated by hbm2java
 */
@Entity
@Table(name="loan"
    ,schema="public"
)
public class Loan  implements java.io.Serializable {


     private long loanid;
     private Borrower borrower;
     private Resource resource;
     private Date borrowed;
     private Date returned;
     private String comment;

    public Loan() {
    }

	
    public Loan(long loanid, Borrower borrower, Resource resource, Date borrowed) {
        this.loanid = loanid;
        this.borrower = borrower;
        this.resource = resource;
        this.borrowed = borrowed;
    }
    public Loan(long loanid, Borrower borrower, Resource resource, Date borrowed, Date returned, String comment) {
       this.loanid = loanid;
       this.borrower = borrower;
       this.resource = resource;
       this.borrowed = borrowed;
       this.returned = returned;
       this.comment = comment;
    }
   
     @Id 

    
    @Column(name="loanid", unique=true, nullable=false, precision=18, scale=0)
    public long getLoanid() {
        return this.loanid;
    }
    
    public void setLoanid(long loanid) {
        this.loanid = loanid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="borrowerid", nullable=false)
    public Borrower getBorrower() {
        return this.borrower;
    }
    
    public void setBorrower(Borrower borrower) {
        this.borrower = borrower;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="resourceid", nullable=false)
    public Resource getResource() {
        return this.resource;
    }
    
    public void setResource(Resource resource) {
        this.resource = resource;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="borrowed", nullable=false, length=29)
    public Date getBorrowed() {
        return this.borrowed;
    }
    
    public void setBorrowed(Date borrowed) {
        this.borrowed = borrowed;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="returned", length=29)
    public Date getReturned() {
        return this.returned;
    }
    
    public void setReturned(Date returned) {
        this.returned = returned;
    }

    
    @Column(name="comment", length=600)
    public String getComment() {
        return this.comment;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }




}


