package org.clibrary.db.sqlbuilder;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.clibrary.CLibraryException;
import org.clibrary.db.hibernate.Borrower;

public class BorrowerHqlBuilder {

	private String lessThanBorrowerId;
	private String largerThanBorrowerId;
	private boolean includeLargerThanBorrowerId;
	private String order = null;
	private List<Object> paramList = null;
	private int paramCounter = 1;

	private void validate(final Borrower borrower) {

		if (StringUtils.isNoneEmpty(lessThanBorrowerId) && StringUtils.isNoneEmpty(largerThanBorrowerId)) {
			throw new CLibraryException("Both lessThanBorrowerId and largerThanBorrowerId cannot be used.");
		}
		
		if (StringUtils.isNoneEmpty(borrower.getBorrowerid()) && StringUtils.isNoneEmpty(lessThanBorrowerId)) {
			throw new CLibraryException("Both Borrower.borrowerId and lessThanBorrowerId cannot be used.");
		}
		
		if (StringUtils.isNoneEmpty(borrower.getBorrowerid()) && StringUtils.isNoneEmpty(largerThanBorrowerId)) {
			throw new CLibraryException("Both Borrower.borrowerId and largerThanBorrowerId cannot be used.");
		}
	}

	/**
	 * Build SQL to find borrowers. The SQL may perform terribly in a large
	 * system if wildcards are used.
	 * 
	 * @param borrower
	 *            If a field is not null or empty, then add a like clause.
	 * @param lessThanBorrowerId
	 *            If not null then add a
	 * @param numRecords
	 */
	public String buildSql(final Borrower borrower) {
		
		if (borrower == null) {
			return null;
		}
		
		validate(borrower);
		paramList = new ArrayList<>();
		
		/*
		 * Create the search string. Set the initial search string buffer size
		 * to 512 bytes.
		 */
		StringBuilder searchStr = new StringBuilder(512);
		boolean usedWhere = false;
		boolean useAnd = false;

		searchStr.append("select b from Borrower b");

		/*
		 * Only add the "where borrowerid < '???'" part if lessThanBorrowerId is
		 * not null.
		 */
		if (lessThanBorrowerId != null) {

			searchStr.append("\nwhere borrowerid < " + getNextParam());
			paramList.add(lessThanBorrowerId);
			usedWhere = true;
			useAnd = true;
		}

		if (largerThanBorrowerId != null) {
			
			if (!usedWhere) {
				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {
				searchStr.append("\nand");
			}
			useAnd = true;

			if (includeLargerThanBorrowerId) {
				searchStr.append(" borrowerid >= " + getNextParam());
			} else {
				searchStr.append(" borrowerid > " + getNextParam());
			}
			paramList.add(largerThanBorrowerId);
		}

		if ((borrower.getBorrowerid() != null) && (borrower.getBorrowerid().length() > 0)) {

			if (!usedWhere) {

				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {

				searchStr.append("\nand");
			}

			searchStr.append(" borrowerid = " + getNextParam());
			paramList.add(borrower.getBorrowerid());
			useAnd = true;
		}

		if ((borrower.getFirstname() != null) && (borrower.getFirstname().length() > 0)) {

			if (!usedWhere) {

				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {

				searchStr.append("\nand");
			}

			searchStr.append(" lower(firstname) like " + getNextParam());
			paramList.add(borrower.getFirstname().toLowerCase());
			useAnd = true;
		}

		if ((borrower.getLastname() != null) && (borrower.getLastname().length() > 0)) {

			if (!usedWhere) {

				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {

				searchStr.append("\nand");
			}

			searchStr.append(" lower(lastname) like " + getNextParam());
			paramList.add(borrower.getLastname().toLowerCase());
			useAnd = true;
		}

		if ((borrower.getAddress() != null) && (borrower.getAddress().length() > 0)) {

			if (!usedWhere) {

				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {

				searchStr.append("\nand");
			}

			searchStr.append(" lower(address) like " + getNextParam());
			paramList.add(borrower.getAddress().toLowerCase());
			useAnd = true;
		}

		if ((borrower.getPhone() != null) && (borrower.getPhone().length() > 0)) {

			if (!usedWhere) {

				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {

				searchStr.append("\nand");
			}

			searchStr.append(" phone like " + getNextParam());
			paramList.add(borrower.getPhone());
			useAnd = true;
		}

		if ((borrower.getMobile() != null) && (borrower.getMobile().length() > 0)) {

			if (!usedWhere) {

				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {

				searchStr.append("\nand");
			}

			searchStr.append(" mobile like " + getNextParam());
			paramList.add(borrower.getMobile());
			useAnd = true;
		}

		if ((borrower.getEmail() != null) && (borrower.getEmail().length() > 0)) {

			if (!usedWhere) {

				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {

				searchStr.append("\nand");
			}

			searchStr.append(" lower(email) like " + getNextParam());
			paramList.add(borrower.getEmail().toLowerCase());
			useAnd = true;
		}

		if ((borrower.getComment() != null) && (borrower.getComment().length() > 0)) {

			if (!usedWhere) {

				searchStr.append("\nwhere");
				usedWhere = true;
			}

			if (useAnd) {

				searchStr.append("\nand");
			}

			searchStr.append(" lower(comment) like " + getNextParam());
			paramList.add(borrower.getComment().toLowerCase());
			useAnd = true;
		}

		searchStr.append("\norder by b.borrowerid");
		if (StringUtils.isNotEmpty(order)) {
			searchStr.append(" ");
			searchStr.append(order);
		}
		
		return searchStr.toString();
	}

	public String getLessThanBorrowerId() {
		return lessThanBorrowerId;
	}

	public void setLessThanBorrowerId(String lessThanBorrowerId) {
		this.lessThanBorrowerId = lessThanBorrowerId;
	}

	public String getLargerThanBorrowerId() {
		return largerThanBorrowerId;
	}

	public void setLargerThanBorrowerId(String largerThanBorrowerId, boolean includeLargerThanBorrowerId) {
		this.largerThanBorrowerId = largerThanBorrowerId;
		this.includeLargerThanBorrowerId = includeLargerThanBorrowerId;
	}

	public boolean isIncludeLargerThanBorrowerId() {
		return includeLargerThanBorrowerId;
	}

	public List<Object> getParamList() {
		return paramList;
	}

	public String getOrder() {
		return order;
	}

	/**
	 * Must be "asc" or "desc".
	 * @param order
	 */
	public void setOrder(String order) {
		this.order = order;
	}
	
	private String getNextParam() {
		
		return "?" + paramCounter++;
	}
}
