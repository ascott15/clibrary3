package org.clibrary.db.sql.springconfig;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.clibrary.db.dao.BorrowerDao;
import org.clibrary.db.dao.CategoryDao;
import org.clibrary.db.dao.LoanDao;
import org.clibrary.db.dao.OwnerDao;
import org.clibrary.db.dao.ResourceDao;
import org.clibrary.db.dao.ResourceTypeDao;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Can use either of the following annotations to define a producer. It appears
 * the EJB 3.1 @Produces annotation is not yet supported.
 * <ul>
 * <li>@Component
 * <li>@Configuration - Uses CGLIB for access. Probably to ensure singleton
 * creation and to allow for circular reference.
 * </ul>
 * Other points:
 * <ul>
 * <li>Can use EJB 3.1 annotations if javax.inject is in the maven depencencies.
 * See http
 * ://static.springsource.org/spring/docs/3.2.x/spring-framework-reference/html
 * /beans.html chapter '5.11.3 Limitations of the standard approach'.
 * <li>Every bean has a default name.
 * </ul>
 * 
 * @author ascott
 * 
 */
@Configuration
@PropertySource("classpath:conf/database.properties")
@EnableTransactionManagement
public class CLibraryDbSpringConfig {

	@Value("${db.connection.url}")
	protected String dbUrl;

	@Value("${db.connection.username}")
	protected String dbUsername;

	@Value("${db.connection.password}")
	protected String dbPassword;

	@Value("${db.connection.driver_class}")
	protected String dbClassName;

	protected HibernateTransactionManager hibernateTransactionManager = null;
	protected SessionFactory sessionFactory = null;
	protected DriverManagerDataSource driverManagerDataSource = null;
	private static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer;
	protected BorrowerDao borrowerDao = null;
	protected CategoryDao categoryDao = null;
	protected LoanDao loanDao = null;
	protected OwnerDao ownerDao = null;
	protected ResourceDao resourceDao = null;
	protected ResourceTypeDao resourceTypeDao = null;

	public CLibraryDbSpringConfig() {
	}

//	// The default name is getHibernateTransactionManager.
//	@Bean(name = "hibernateTransactionManager")
//	public HibernateTransactionManager getHibernateTransactionManager() {
//
//		if (hibernateTransactionManager == null) {
//
//			hibernateTransactionManager = new HibernateTransactionManager(getSessionFactory());
//		}
//
//		return hibernateTransactionManager;
//	}

//	// The default name is getSessionFactory.
//	@Bean(name = "sessionFactory")
//	public SessionFactory getSessionFactory() {
//
//		if (sessionFactory == null) {
//
//			/*
//			 * These properties are optional.
//			 */
//			Properties properties = new Properties();
//			try (InputStream inStream = Thread.currentThread().getContextClassLoader()
//					.getResourceAsStream("hibernate.properties")) {
//
//				if (inStream != null) {
//
//					properties.load(inStream);
//				}
//
//			} catch (IOException ex) {
//
//				throw new RuntimeException(ex);
//			}
//
//			LocalSessionFactoryBuilder localSessionFactoryBuilder = new LocalSessionFactoryBuilder(getDataSource());
//			localSessionFactoryBuilder.scanPackages("au.net.ascott.clibrary3.db.hibernate",
//					"au.net.ascott.clibrary3.db.hibernatequeries");
//			localSessionFactoryBuilder.addProperties(properties);
//			sessionFactory = localSessionFactoryBuilder.buildSessionFactory();
//
//		}
//
//		return sessionFactory;
//	}
	
	private JpaTransactionManager jpaTransactionManager;
	
	@Bean(name = "jpaTransactionManager")
	public JpaTransactionManager getJpaTransactionManager() {
		
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(getEntityManagerFactory());
		
		return jpaTransactionManager;
	}
	
	private EntityManagerFactory entityManagerFactory;
	
	@Bean(name = "entityManagerFactory") 
	public EntityManagerFactory getEntityManagerFactory() {
		
		
		LocalContainerEntityManagerFactoryBean localEntityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		localEntityManagerFactory.setPackagesToScan("org.clibrary.db.hibernate",
				"org.clibrary.db.hibernatequeries");
		localEntityManagerFactory.setDataSource(getDataSource());
		localEntityManagerFactory.setPersistenceProvider(new HibernatePersistenceProvider());
		localEntityManagerFactory.afterPropertiesSet();
		
		entityManagerFactory = localEntityManagerFactory.getNativeEntityManagerFactory();
		
		return entityManagerFactory;
	}

	// The default name is getDataSource
	@Bean(name = "dataSource")
	public DataSource getDataSource() {

		if (driverManagerDataSource == null) {

			driverManagerDataSource = new DriverManagerDataSource(dbUrl, dbUsername, dbPassword);
			driverManagerDataSource.setDriverClassName(dbClassName);
		}

		return driverManagerDataSource;
	}
	
//	// The default name is getMovieDetailsDao.
//	@Bean(name = "borrowerDao")
//	public BorrowerDao getBorrowerDao() {
//
//		if (borrowerDao == null) {
//
//			borrowerDao = new BorrowerDaoImpl(getSessionFactory());
//		}
//
//		return borrowerDao;
//	}
//	
//	// The default name is getMovieDetailsDao.
//	@Bean(name = "categoryDao")
//	public CategoryDao getCategoryDao() {
//
//		if (categoryDao == null) {
//
//			categoryDao = new CategoryDaoImpl(getSessionFactory());
//		}
//
//		return categoryDao;
//	}
//	
//	// The default name is getMovieDetailsDao.
//	@Bean(name = "loanDao")
//	public LoanDao getLoanDao() {
//
//		if (loanDao == null) {
//
//			loanDao = new LoanDaoImpl(getSessionFactory());
//		}
//
//		return loanDao;
//	}
//	
//	// The default name is getMovieDetailsDao.
//	@Bean(name = "ownerDao")
//	public OwnerDao getOwnerDao() {
//
//		if (ownerDao == null) {
//
//			ownerDao = new OwnerDaoImpl(getSessionFactory());
//		}
//
//		return ownerDao;
//	}
//	
//	// The default name is getMovieDetailsDao.
//	@Bean(name = "resourceDao")
//	public ResourceDao getResourceDao() {
//
//		if (resourceDao == null) {
//
//			resourceDao = new ResourceDaoImpl(getSessionFactory());
//		}
//
//		return resourceDao;
//	}
//
//	// The default name is getMovieDetailsDao.
//	@Bean(name = "resourceTypeDao")
//	public ResourceTypeDao getResourceTypeDao() {
//
//		if (resourceTypeDao == null) {
//
//			resourceTypeDao = new ResourceTypeDaoImpl(getSessionFactory());
//		}
//
//		return resourceTypeDao;
//	}

	/**
	 * Must be static for @Value to work.
	 * 
	 * @return
	 */
	// The default name is getAllHomesUrlStr.
	@Bean(name = "getPropertySourcesPlaceholderConfigurer")
	public static PropertySourcesPlaceholderConfigurer getPropertySourcesPlaceholderConfigurer() {

		if (propertySourcesPlaceholderConfigurer == null) {

			propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
		}

		return propertySourcesPlaceholderConfigurer;
	}
}
