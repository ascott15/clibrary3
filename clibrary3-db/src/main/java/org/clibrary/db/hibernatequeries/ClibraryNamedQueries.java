package org.clibrary.db.hibernatequeries;

import javax.persistence.MappedSuperclass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({
	/*
	 * Borrower.
	 */
	@NamedQuery(name = "Borrower.findAll", query = "select b from Borrower b order by b.borrowerid"),
	@NamedQuery(name = "Borrower.nextSeqVal.h2", query = "select b from Borrower b order by b.borrowerid"),
	/*
	 * Category.
	 */
	@NamedQuery(name = "Category.findAll", query = "select c from Category c order by c.categoryname"),
	/*
	 * Loan.
	 */
	@NamedQuery(name = "Loan.findAll", query = "select l from Loan l order by l.loanid"),
	/*
	 * Owner.
	 */
	@NamedQuery(name = "Owner.findAll", query = "select o from Owner o order by o.ownerid"),
	/*
	 * ResourceType.
	 */
	@NamedQuery(name = "Resource.findAll", query = "select r from Resource r order by r.resourceid"),
	/*
	 * ResourceType.
	 */
	@NamedQuery(name = "ResourceType.findAll", query = "select r from Resourcetype r order by r.resourcetype")
	})
@MappedSuperclass
public interface ClibraryNamedQueries {
	
}
