package org.clibrary.db.dao;

import org.clibrary.db.hibernate.Loan;

/**
 * Spring recommends that you only annotate concrete classes (and methods of
 * concrete classes) with the @Transactional.
 * 
 * @author ascott
 * 
 */
public interface LoanDao extends GenericDao<Loan, Integer> {
}
