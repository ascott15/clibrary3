package org.clibrary.db.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;

public abstract class GenericDaoImpl<T, ID extends Serializable> implements GenericDao<T, ID> {

	private final Class<T> persistentClass;
	protected final EntityManager entityManager;
	
	public GenericDaoImpl(final EntityManager entityManager) {

		this.entityManager = entityManager;

		persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}
}
