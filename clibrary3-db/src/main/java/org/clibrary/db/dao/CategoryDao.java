package org.clibrary.db.dao;

import org.clibrary.db.hibernate.Category;

/**
 * Spring recommends that you only annotate concrete classes (and methods of
 * concrete classes) with the @Transactional.
 * 
 * @author ascott
 * 
 */
public interface CategoryDao extends GenericDao<Category, Integer> {
}
