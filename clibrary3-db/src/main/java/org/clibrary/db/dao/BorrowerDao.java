package org.clibrary.db.dao;

import java.util.List;

import org.clibrary.db.hibernate.Borrower;

/**
 * Spring recommends that you only annotate concrete classes (and methods of
 * concrete classes) with the @Transactional.
 * 
 * @author ascott
 * 
 */
public interface BorrowerDao extends GenericDao<Borrower, String> {
	
	public List<Borrower> find(final Borrower borrower, final int numRecords);
	
	public List<Borrower> findBackward(final Borrower borrower, final String lessThanBorrowerId, final int numRecords);
	
	public List<Borrower> findForward(final Borrower borrower, final String largerThanBorrowerId,
			final boolean includeLargerThanBorrowerId, final int numRecords);
}
