package org.clibrary.db.dao;

import org.clibrary.db.hibernate.Owner;

/**
 * Spring recommends that you only annotate concrete classes (and methods of
 * concrete classes) with the @Transactional.
 * 
 * @author ascott
 * 
 */
public interface OwnerDao extends GenericDao<Owner, Integer> {
}
