package org.clibrary.db.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.clibrary.db.hibernate.Borrower;
import org.clibrary.db.sqlbuilder.BorrowerHqlBuilder;

public class BorrowerDaoImpl extends GenericDaoImpl<Borrower, String> implements BorrowerDao {

	/**
	 * A borrower ID is prefixed with 'B' followed by a minimum of 5 digits.
	 */
//	private final DecimalFormat idFormat = new DecimalFormat("B##########00000");

	public BorrowerDaoImpl(EntityManager entityManager) {

		super(entityManager);
	}

//	@Override
//	//@Transactional
//	public void save(final Borrower borrower) {
//
//		/*
//		 * I was hoping to use @ValueGenerationType to generate the borrower ID
//		 * but this did not work.
//		 */
//		if (borrower.getBorrowerid() == null) {
//			String borrowerId = idFormat.format(getNextBorrowerId());
//			borrower.setBorrowerid(borrowerId);
//		}
//		super.save(borrower);
//	}

	@Override
	public List<Borrower> findAll() {

		Query query = entityManager.createNamedQuery("Borrower.findAll");
		@SuppressWarnings("unchecked")
		List<Borrower> BorrowerList = query.getResultList();

		return BorrowerList;
	}

	@Override
	public List<Borrower> find(final Borrower borrower, final int numRecords) {

		BorrowerHqlBuilder borrowerHqlBuilder = new BorrowerHqlBuilder();
		String hql = borrowerHqlBuilder.buildSql(borrower);
		Query query = entityManager.createQuery(hql, Borrower.class);
		int index = 0;
		for (Object obj : borrowerHqlBuilder.getParamList()) {
			query.setParameter(index++, obj);
		}
		query.setMaxResults(numRecords);
		List<Borrower> borrowerList = query.getResultList();
		return borrowerList;
	}

	@Override
	public List<Borrower> findBackward(final Borrower borrower, final String lessThanBorrowerId, final int numRecords) {

		BorrowerHqlBuilder borrowerHqlBuilder = new BorrowerHqlBuilder();
		borrowerHqlBuilder.setLessThanBorrowerId(lessThanBorrowerId);
		borrowerHqlBuilder.setOrder("desc");
		String hql = borrowerHqlBuilder.buildSql(borrower);
		Query query = entityManager.createQuery(hql, Borrower.class);
		int index = 1;
		for (Object obj : borrowerHqlBuilder.getParamList()) {
			query.setParameter(index++, obj);
		}
		query.setMaxResults(numRecords);
		List<Borrower> borrowerList = query.getResultList();
		return borrowerList;
	}

	@Override
	public List<Borrower> findForward(final Borrower borrower, final String largerThanBorrowerId,
			final boolean includeLargerThanBorrowerId, final int numRecords) {

		BorrowerHqlBuilder borrowerHqlBuilder = new BorrowerHqlBuilder();
		borrowerHqlBuilder.setLargerThanBorrowerId(largerThanBorrowerId, includeLargerThanBorrowerId);
		String hql = borrowerHqlBuilder.buildSql(borrower);
		Query query = entityManager.createQuery(hql, Borrower.class);
		int index = 1;
		for (Object obj : borrowerHqlBuilder.getParamList()) {
			if (obj == null) {
				throw new RuntimeException("Missing HQL parameter " + (index + 1));
			}
			query.setParameter(index++, obj);
		}
		query.setMaxResults(numRecords);
		List<Borrower> borrowerList = query.getResultList();
		return borrowerList;
	}

	/**
	 * Get the next borrower ID from the BorrowerIdGen sequence. The old way of
	 * getting the borrower ID was to get the maximum previous borrower ID. The
	 * old way was not safe if there were multiple concurrent users.
	 * 
	 * @return
	 */
//	private long getNextBorrowerId() {
//
//		ReturningWork<Long> returningWork = new ReturningWork<Long>() {
//
//			@Override
//			public Long execute(Connection connection) throws SQLException {
//
//				DialectResolver dialectResolver = new StandardDialectResolver();
//				Dialect dialect = dialectResolver
//						.resolveDialect(new DatabaseMetaDataDialectResolutionInfoAdapter(connection.getMetaData()));
//				try (PreparedStatement preparedStmt = connection
//						.prepareStatement(dialect.getSequenceNextValString("BorrowerIdGen"))) {
//					ResultSet resultSet = preparedStmt.executeQuery();
//					resultSet.next();
//					return resultSet.getLong(1);
//				}
//
//			}
//		};
//
//		Long nextId = sessionFactory.getCurrentSession().doReturningWork(returningWork);
//		return nextId;
//	}
}
