package org.clibrary.db.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.clibrary.db.hibernate.Resourcetype;

public class ResourceTypeDaoImpl extends GenericDaoImpl<Resourcetype, Integer>
		implements ResourceTypeDao {
	
	public ResourceTypeDaoImpl(EntityManager entityManager) {

		super(entityManager);
	}
	
	public List<Resourcetype> findAll() {
		
		Query query = entityManager.createNamedQuery("ResourceType.findAll");
		@SuppressWarnings("unchecked")
		List<Resourcetype> ResourcetypeList = query.getResultList();

		return ResourcetypeList;
	}
}
