package org.clibrary.db.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.clibrary.db.hibernate.Loan;

public class LoanDaoImpl extends GenericDaoImpl<Loan, Integer>
		implements LoanDao {
	
	public LoanDaoImpl(EntityManager entityManager) {

		super(entityManager);
	}
	
	public List<Loan> findAll() {
		
		Query query = entityManager.createNamedQuery("Loan.findAll");
		@SuppressWarnings("unchecked")
		List<Loan> LoanList = query.getResultList();

		return LoanList;
	}
}
