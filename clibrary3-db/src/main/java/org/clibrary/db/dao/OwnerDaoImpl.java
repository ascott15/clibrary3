package org.clibrary.db.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.clibrary.db.hibernate.Owner;

public class OwnerDaoImpl extends GenericDaoImpl<Owner, Integer>
		implements OwnerDao {
	
	public OwnerDaoImpl(EntityManager entityManager) {

		super(entityManager);
	}
	
	public List<Owner> findAll() {
		
		Query query = entityManager.createNamedQuery("Owner.findAll");
		@SuppressWarnings("unchecked")
		List<Owner> OwnerList = query.getResultList();

		return OwnerList;
	}
}
