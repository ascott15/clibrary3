package org.clibrary.db.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.clibrary.db.hibernate.Category;

public class CategoryDaoImpl extends GenericDaoImpl<Category, Integer>
		implements CategoryDao {
	
	public CategoryDaoImpl(EntityManager entityManager) {

		super(entityManager);
	}
	
	public List<Category> findAll() {
		
		Query query = entityManager.createNamedQuery("Category.findAll");
		@SuppressWarnings("unchecked")
		List<Category> CategoryList = query.getResultList();

		return CategoryList;
	}
}
