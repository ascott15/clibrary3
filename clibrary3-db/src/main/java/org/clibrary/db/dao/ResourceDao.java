package org.clibrary.db.dao;

import org.clibrary.db.hibernate.Resource;

/**
 * Spring recommends that you only annotate concrete classes (and methods of
 * concrete classes) with the @Transactional.
 * 
 * @author ascott
 * 
 */
public interface ResourceDao extends GenericDao<Resource, Integer> {
}
