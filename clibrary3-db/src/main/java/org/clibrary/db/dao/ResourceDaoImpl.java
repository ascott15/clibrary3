package org.clibrary.db.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.clibrary.db.hibernate.Resource;

public class ResourceDaoImpl extends GenericDaoImpl<Resource, Integer>
		implements ResourceDao {
	
	public ResourceDaoImpl(EntityManager entityManager) {

		super(entityManager);
	}
	
	public List<Resource> findAll() {
		
		Query query = entityManager.createNamedQuery("Resource.findAll");
		@SuppressWarnings("unchecked")
		List<Resource> ResourceList = query.getResultList();

		return ResourceList;
	}
}
