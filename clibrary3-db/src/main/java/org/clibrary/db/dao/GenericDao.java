package org.clibrary.db.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;

/**
 * Spring recommends that you only annotate concrete classes (and methods of
 * concrete classes) with the @Transactional.
 * 
 * @author ascott
 * 
 */
public interface GenericDao<T, ID extends Serializable> {

    /**
     * Retrieve all objects and all properties of all objects, including lazy
     * properties.
     * 
     * @return
     */
    public List<T> findAll();
}
