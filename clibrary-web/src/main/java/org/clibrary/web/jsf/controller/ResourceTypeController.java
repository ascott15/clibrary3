/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.controller;

import org.clibrary.db.hibernate.Resourcetype;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.ResizeEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author ascott
 */
@Named(value = "myResourceTypeController")
@SessionScoped
public class ResourceTypeController implements Serializable {
 
    @EJB
    private org.clibrary.db.bean.ResourceTypeFacade ejbFacade;
    
    static final long serialVersionUID = -1L;

    private Resourcetype current = new Resourcetype();
    private List<Resourcetype> ResourceTypePageList = new ArrayList<>();
    
    private static final boolean error = false;

    /**
     * Creates a new instance of AddResourceType
     */
    public ResourceTypeController() {
    }
    
    /**
     * Saves a new ResourceType
     * @return a string
     */
    public String save() {
        try{
            System.out.println("rt con save first "+ current.getResourcetype());
            ejbFacade.create(current); //Resourcetype.java called ere this call
            System.out.println("rt con save 2nd");
            current = new Resourcetype();
            FacesMessage msg = new FacesMessage("Saved");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e){
            FacesMessage msg = new FacesMessage("Error saving - reason: " + e);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        return "addResourceType";
    }
    
    public void onRowEdit(RowEditEvent event) {

        Object obj = event.getObject();
        Resourcetype resourceType = (Resourcetype) obj;
        ejbFacade.edit(resourceType);
    }
    
    public void onRowSelect(SelectEvent event) {
        Object obj = event.getObject();
        Resourcetype resource = (Resourcetype) obj;
        ejbFacade.edit(resource);
    }
    
    public void onRowEditCancel(RowEditEvent event) {

        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onCellEdit(CellEditEvent event) {       
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();       
        if(newValue != null && !newValue.equals(oldValue)) {          
            System.out.println("rc onCellEdit Changed rowkey=" + event.getRowKey() + " field=" + event.getColumn().getField() + " " + event.getNewValue().toString());
        }       
    }
    
    public String loadResourceTypes() {
        ResourceTypePageList.clear();
        //On a Windows PC:\Users\DL\workspace\clibrary3-db\src\main\java\org\clibrary\db\hibernatequeries
        ResourceTypePageList.addAll(ejbFacade.findAll());
        
        Collections.sort(ResourceTypePageList, new Comparator<Resourcetype>(){
            @Override
            public int compare(Resourcetype t, Resourcetype t1) {
                return t.getResourcetype().compareToIgnoreCase(t1.getResourcetype());
            }
        });
        return "";
    }

    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the current ResourceType
     */
    public Resourcetype getCurrent() {
        return current;
    }

    /**
     * @param current the current ResourceType to set
     */
    public void setCurrent(Resourcetype current) {
        this.current = current;
    }

    /**
     * @return the ResourceTypePageList
     */
    public List<Resourcetype> getResourceTypePageList() {
        loadResourceTypes();
        return ResourceTypePageList;
    }

    /**
     * @param ResourceTypePageList the ResourceTypePageList to set
     */
    public void setResourceTypePageList(List<Resourcetype> ResourceTypePageList) {
        this.ResourceTypePageList = ResourceTypePageList;
    }
}
