/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.model;

import javax.inject.Named;
import javax.validation.constraints.NotNull;
import org.clibrary.db.hibernate.Resourcetype;

/**
 *
 * @author ascott
 */
@Named
public class CLibResource {
    
    private boolean selected;

    @NotNull
    private String resourceid ;
    private String resourcename ;
    private String isbn ;
    private Category category;
    private Resourcetype resourcetype;
    private String authorfirstname;
    private String authormiddlename;
    private String authorlastname;
    private String comment;
    private char state;
    //private Set<org.clibrary.db.hibernate.Loan> loans = new HashSet<>(0);

    public void reset() {
        resourceid = null;
        resourcename  = null;
        isbn= null;
        category = null;
        resourcetype = null;
        authorfirstname = null;
        authormiddlename = null;
        authorlastname = null;
        comment = null;
        state = 'A';
    }

    public String getResourceId () {
        return resourceid ;
    }

    /**
     * @param ResourceID  the ResourceID to set
     */
    public void setResourceId (String ResourceID) {
        this.resourceid  = ResourceID ;
    }
    
    public String getResourceName () {
        return resourcename ;
    }

    /**
     * @param ResourceName  the ResourceName to set
     */
    public void setResourceName (String ResourceName) {
        this.resourcename  = ResourceName ;
    }

    public String getISBN () {
        return isbn ;
    }
    
    /**
     * @param ISBN  the ResourceName to set
     */
    public void setisbn (String ISBN) {
        this.isbn  = ISBN ;
    }

    public Category getCategory() {
        return this.category;
    }
    
    public void setCategory(Category category) {
        this.category = category;
    }
    
    public Resourcetype getResourcetype() {
        return this.resourcetype;
    }
    
    public void setResourcetype(Resourcetype resourcetype) {
        this.resourcetype = resourcetype;
    }
   
    public String getAuthorfirstname() {
        return this.authorfirstname;
    }
    
    public void setAuthorfirstname(String authorfirstname) {
        this.authorfirstname = authorfirstname;
    }
    
    public String getAuthormiddlename() {
        return this.authormiddlename;
    }
    
    public void setAuthormiddlename(String authormiddlename) {
        this.authormiddlename = authormiddlename;
    }

    public String getAuthorlastname() {
        return this.authorlastname;
    }
    
    public void setAuthorlastname(String authorlastname) {
        this.authorlastname = authorlastname;
    }

    public String getComment() {
        return this.comment;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public char getState() {
        return this.state;
    }
    
    public void setState(char state) {
        this.state = state;
    }
    
    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
