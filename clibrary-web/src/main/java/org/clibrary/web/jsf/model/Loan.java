/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.model;

import java.util.Date;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ascott
 */
@Named
public class Loan {
    
    private boolean selected;

    @NotNull
    private Date Borrowed ;
    
    @NotNull
    private Date Returned ;
    
    @NotNull
    private String Borrower;
    
    @NotNull
    private String ResourceID ;
    
    @NotNull
    private String Comment ;

    public void reset() {
        Borrowed  = null;
        Returned = null;
        Borrower = null;
        ResourceID = null;
        Comment = null;
    }

    public Date getBorrowed () {
        return Borrowed ;
    }

    /**
     * @param Borrowed  the date borrowed to set
     */
    public void setBorrowed (Date Borrowed) {
        this.Borrowed  = Borrowed ;
    }

    public Date getReturned () {
        return Returned ;
    }

    /**
     * @param Reurned  the date borrowed to set
     */
    public void setReturned (Date Returned) {
        this.Returned  = Returned ;
    }
    
    public String getBorrower () {
        return Borrower;
    }

    /**
     * @param Borrowed  the date borrowed to set
     */
    public void setBorrowerID (String Borrower) {
        this.Borrower  = Borrower ;
    }
    
    public String getResourceID () {
        return ResourceID ;
    }

    /**
     * @param Borrowed  the date borrowed to set
     */
    public void setResourceID (String ResourceID) {
        this.ResourceID  = ResourceID ;
    }
    
    public String getComment () {
        return Comment ;
    }

    /**
     * @param Borrowed  the date borrowed to set
     */
    public void setComment (String Comment) {
        this.Comment  = Comment ;
    }
    
    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
