/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.controller;

import org.clibrary.db.hibernate.Category;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.ResizeEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author ascott
 */
@Named(value = "myCategoryController")
@SessionScoped
public class CategoryController implements Serializable {
 
    @EJB
    private org.clibrary.db.bean.CategoryFacade ejbFacade;
    static final long serialVersionUID = -1L;
    private Category current = new Category();
    private List<Category> categoryPageList = new ArrayList<>();
    private final String instructions = "none";
    private static final boolean error = false;

    /**
     * Creates a new instance of AddCategory
     */
    public CategoryController() {
    }
    
    /**
     * Saves a new Category
     * @return a string
     */  
    public String Save() {
        try{
            ejbFacade.create(current); //Category.java called ere this call
            current = new Category();
            FacesMessage msg = new FacesMessage("Category " + current.getCategoryName() + " created");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e){
            FacesMessage msg;
            int a = e.getCause().getCause().getCause().getCause().getLocalizedMessage().indexOf("Detail") +12;
            if(a > 0){
                String b = e.getCause().getCause().getCause().getCause().getLocalizedMessage().substring(a);
                msg = new FacesMessage("Error: " + b);
            }else {
                msg = new FacesMessage(e.toString());
            }
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        return "";
    }
    
    public String getInstructions(){ 
        return instructions;
    } //provides instructions on how to delete/update categories which are assigned to resources
    
    /*public List<String> getUnusedCategoryList(){
        int counter = 0;
        ResultSet resultset = null;
        PreparedStatement preparedStatement = null;
        try{
            Connection connection = DBConn.getDBConn();
            preparedStatement = connection.prepareStatement("Select \"Category\".\"CategoryName\" "
             + "from public.\"Category\" " +
            "where \"Category\".\"CategoryID\" not in( " +
            "select \"Resource\".\"CategoryID\" " +
            "from public.\"Resource\")");
            unusedCategoryList.clear();
            resultset = preparedStatement.executeQuery();
            while(resultset.next()){
                unusedCategoryList.add(resultset.getString(1));
                counter++;
            } 
            
            if(counter==0){
               instructions =  "There are no categories you can currently delete";
            } else{
               instructions = "NB You can only remove or change the following categories (those that are not assigned to resources). To change other categories go to the 'Resources by Category' report, note all the resources which have the category you want to change, then go to the 'Browse Resources' form and give those resources a different category. Then return to this form and change or remove the category.";
            }
            System.out.println("cc ctr="+ counter + " " + instructions);
        } catch(SQLException ex){
            String msg = "";
            Throwable cause = ex.getCause();
            if (cause != null) {
                        msg = cause.getLocalizedMessage();
            }
            if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
            } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("SQLError"));
            }
        } finally{
            try{
                if(!resultset.isClosed()){
                    resultset.close();
                }
                preparedStatement.close();
            } catch(SQLException ex){
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("SQLError"));
                }
            } catch(Exception ex){
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("SQLError"));
                }
            }
           
        } 
        return unusedCategoryList;
    made redundant when editCategories.xhtml finally enabled users to delete categories
    }*/
    
    public String Delete(){
        try{
            ejbFacade.remove(getSelectedCategory());
            FacesMessage msg = new FacesMessage("Category '" + current.getCategoryName() + "' deleted");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch(Exception e){
            FacesMessage msg;
            int a = e.getCause().getCause().getCause().getCause().getLocalizedMessage().indexOf("Detail") +12;
            if(a > 0){
                String b = e.getCause().getCause().getCause().getCause().getLocalizedMessage().substring(a);
                msg = new FacesMessage("Error: " + b);
            }else {
                msg = new FacesMessage(e.toString());
            }
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }    
        return "";
    }
    
    public String Update(String cat){
        try{
        ejbFacade.edit(current);
        } catch(Exception e){
             FacesMessage msg;
            int a = e.getCause().getCause().getCause().getCause().getLocalizedMessage().indexOf("Detail") +12;
            if(a > 0){
                String b = e.getCause().getCause().getCause().getCause().getLocalizedMessage().substring(a);
                msg = new FacesMessage("Error: " + b);
            }else {
                msg = new FacesMessage(e.toString());
            }
            FacesContext.getCurrentInstance().addMessage(null, msg);       
        }
        return "";
    }
    
    public void onRowSelect(SelectEvent event) {
        Object obj = event.getObject();
        Category category = (Category) obj;
        ejbFacade.edit(category);
    }
    
    public void onRowEdit(RowEditEvent event) {
        Object obj = event.getObject();
        Category category = (Category) obj;
        ejbFacade.edit(category);
        System.out.println("cc onrowEdit " + event.getComponent().getId() + " "  + event.getSource().toString());
    }
    
    public void onRowEditCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onCellEdit(CellEditEvent event) {       
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();       
        if(newValue != null && !newValue.equals(oldValue)) {          
        }       
    }
    
    private Category selectedCategory;
    public Category getSelectedCategory(){
        return selectedCategory;
    }
    
    public void setSelectedCategory(Category selectedCategory){
        this.selectedCategory = selectedCategory;
    }
    
    public String loadCategories() {
        categoryPageList.clear();
        //On a Windows PC:\Users\DL\workspace\clibrary3-db\src\main\java\org\clibrary\db\hibernatequeries
        categoryPageList.addAll(ejbFacade.findAll());
        return "";
    }

    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the current category
     */
    public Category getCurrent() {
        return current;
    }

    /**
     * @param current the current category to set
     */
    public void setCurrent(Category current) {
        this.current = current;
    }

    /**
     * @return the categoryPageList
     */
    public List<Category> getCategoryCategoryPageList() {
        loadCategories();
        //Comparator<? super Category> cmprtr = null;
        Collections.sort(categoryPageList, new Comparator<Category>(){
            @Override
            public int compare(Category t, Category t1) {
                return t.getCategoryName().compareToIgnoreCase(t1.getCategoryName());
            }
        });
        return categoryPageList;
    }

    /**
     * @param categoryPageList the categoryategoryPageList to set
     */
    public void setCategoryPageList(List<Category> categoryPageList) { 
        this.categoryPageList = categoryPageList;
    }
}
