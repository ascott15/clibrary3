/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.controller;

import org.clibrary.web.jsf.model.Borrower;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ascott
 */
public class BorrowerDaoMock {

    private List<Borrower> borrowerList = null;
    
    public List<Borrower> getBorrower(int start, int end) {
        
        List<Borrower> borrowerSubList = new ArrayList<>();
        for (int index = 0; index < borrowerList.size() && index <= end; index++) {
            if (index >= start) {
                borrowerSubList.add(borrowerList.get(index));
            }
        }
        return borrowerList;
    }

    public List<Borrower> getAllBorrowers() {
        
        if (borrowerList != null) {
            
            return borrowerList;
        }

        borrowerList = new ArrayList<>();
        {
            Borrower borrower = new Borrower();
            borrower.setFirstname("John");
            borrower.setLastname("Smith");
            borrower.setAddress("22 Somewhere St, Someplace");
            borrower.setHomePhone("0744441111");
            borrower.setMobile("0416 111 222");
            borrower.setEmail("John.Smith@EmailCompany.com.au");
            borrower.setComment("Is moving soon.");
            borrowerList.add(borrower);
        }
        {
            Borrower borrower = new Borrower();
            borrower.setFirstname("Jane");
            borrower.setLastname("Smith");
            borrower.setAddress("22 Somewhere St, Someplace");
            borrower.setHomePhone("0744441111");
            borrower.setMobile("0416 111 333");
            borrower.setEmail("Jane.Smith@EmailCompany.com.au");
            borrower.setComment("Is moving soon.");
            borrowerList.add(borrower);
        }
        {
            Borrower borrower = new Borrower();
            borrower.setFirstname("Susan");
            borrower.setLastname("Laaks");
            borrower.setAddress("94 Elsewhere St, Someplace");
            borrower.setHomePhone("0744442222");
            borrower.setMobile("0416 222 111");
            borrower.setEmail("Susan_Laaks@EmailOrg.net.au");
            borrower.setComment("Waiting for the new Inspired Living book.");
            borrowerList.add(borrower);
        }
        {
            Borrower borrower = new Borrower();
            borrower.setFirstname("Frank");
            borrower.setLastname("Tens");
            borrower.setAddress("104 Nowhere St, Elsewhere");
            borrower.setHomePhone("0744443333");
            borrower.setMobile("0416 222 222");
            borrower.setEmail(null);
            borrower.setComment(null);
            borrowerList.add(borrower);
        }
        {
            Borrower borrower = new Borrower();
            borrower.setFirstname("John");
            borrower.setLastname("Shen");
            borrower.setAddress("18 Long St, Brisbane");
            borrower.setHomePhone("0744444444");
            borrower.setMobile("0416 222 333");
            borrower.setEmail(null);
            borrower.setComment(null);
            borrowerList.add(borrower);
        }

        return borrowerList;
    }
}
