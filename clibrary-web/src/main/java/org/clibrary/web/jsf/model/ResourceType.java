/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.model;

import javax.inject.Named;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ascott
 */
@Named
public class ResourceType {
    
    private boolean selected;

    @NotNull
    private String ResourceType ;

    public void reset() {
        ResourceType  = null;
    }

    public String getResourceType () {
        return ResourceType ;
    }

    /**
     * @param ResourceType  the ResourceType to set
     */
    public void setResourceType (String ResourceType) {
        this.ResourceType  = ResourceType ;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}