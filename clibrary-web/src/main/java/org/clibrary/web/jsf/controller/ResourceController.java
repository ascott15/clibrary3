/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.controller;

import org.clibrary.db.hibernate.CLibResource;
//import org.clibrary.web.jsf.model.CLibResource; //java.lang.IllegalArgumentException: Not an entity: class org.clibrary.web.jsf.model.CLibResource
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.clibrary.db.hibernate.Category;
import org.clibrary.db.jsf.DBConn;
import org.clibrary.db.jsf.util.JsfUtil;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.ResizeEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.clibrary.db.jsf.TwoStrings.TwoStringsRecord;

/**
 *
 * @author ascott
 */
@Named(value = "myResourceController") //CDI(Weld is an implementaion of this)
@SessionScoped
public class ResourceController implements Serializable {
 
    @EJB
    private org.clibrary.db.bean.ResourceFacade ejbFacade;
    static final long serialVersionUID = -1L;
    private Category selectedCategory;
    private final List<String> StringList = new ArrayList<>(); //used for storing resource types & loan history. 
    TwoStringsRecord twoStringsRecord = null;
    int loanHxCounter=0;
    private CLibResource current = new CLibResource(); //uses .db.hibernate.CLibResource.java
    private List<CLibResource> resourcePageList = new ArrayList<>();
    private static final boolean error = false;
    
    public TwoStringsRecord getList() {
        if(loanHxCounter == 0){
            twoStringsRecord = new TwoStringsRecord("No loans were found for: ", resourcePageList.get(0).getResourceid());
        }
        return twoStringsRecord;
    }

    public void setList() {
        //this.selected = selected;
    }
   
    /**
     * Creates a new instance of AddResource
     */
    public ResourceController() {
    } //When adding a resource this is called first.
    
    public String LoanHistory(){
        ResultSet resultset = null;
        PreparedStatement preparedStatement = null;
        try{
            Connection connection = DBConn.getDBConn();
            preparedStatement = connection.prepareStatement("select \"Borrower\".\"FirstName\",\"Borrower\".\"LastName\", to_char(\"Loan\".\"Borrowed\",'D Mon YYYY')" +
            "from public.\"Loan\", public.\"Resource\", public.\"Borrower\" " +
            "WHERE public.\"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" " +
            "AND public.\"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" " +
            "AND \"Resource\".\"ResourceID\" = ?");
            preparedStatement.setString(1,getSelectedCLibResource().getResourceid());
            resultset = preparedStatement.executeQuery();
            StringList.clear();
            while(resultset.next()){
                twoStringsRecord = new TwoStringsRecord(resultset.getString(1).concat(" ").concat(resultset.getString(2)),resultset.getString(3));
                loanHxCounter ++;
            }     
        } catch(SQLException ex){
            String msg = "";
            Throwable cause = ex.getCause();
            if (cause != null) {
                        msg = cause.getLocalizedMessage();
            }
            if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
            } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("SQLError"));
            }
        } finally{
            try{
                resultset.close();
                preparedStatement.close();
            } catch(SQLException ex){
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("SQLError"));
                }
            }
        }
        return "LoanHx.xhtml";
    }
    /**
     * Saves a new Resource
     * @return a string
     */
    public String Save() {
        String returnValue="";
        FacesMessage msg;
        int numResourcesAdded=0, counter=0;
        ResultSet resultSet;
        try{  
            //ejbFacade.create(current); // causes error: org.postgresql.util.PSQLException: ERROR: invalid byte sequence for encoding "UTF8": 0x00
            //current = new CLibResource();
            //resourcePageList.clear(); //cear the way for a new resource to be added
        //} catch(Exception e){
            PreparedStatement preparedstatement = null;            
            //try{
                Connection connection = DBConn.getDBConn();
                //Check if this resource has not been returned before being presented as a new loan
                //Could this be picked up when an Overdue Loan report is produced and a borrower claims they've already returned the resource? Maybe a query to check the entire loan history for that resource would expose the problem.
                preparedstatement = connection.prepareStatement("select count(*) from public.\"Loan\" WHERE \"Loan\".\"ResourceID\" = ? AND \"Loan\".\"Returned\" is null");
                preparedstatement.setString(1,current.getResourceid());
                resultSet = preparedstatement.executeQuery();
                while(resultSet.next()){
                    counter++;
                }
                if(counter != 0){
                     System.out.println("save ctr !=0");
                    preparedstatement = connection.prepareStatement("INSERT INTO public.\"Resource\" (\"ResourceID\", \"ResourceName\", \"ISBN\", \"ResourceType\", \"CategoryID\", \"AuthorFirstName\", \"AuthorMiddleName\", \"AuthorLastName\", \"Comment\")  values(?,?,?,?,?,?,?,?,?)");
                    preparedstatement.setString(1,current.getResourceid());
                     System.out.println("save 0");
                    preparedstatement.setString(2,current.getResourcename());
                    System.out.println("save 1");
                    preparedstatement.setString(3,current.getIsbn()); 
                     System.out.println("save 2");
                     System.out.println("save " + current.getResourcetype().getResourcetype());
                    preparedstatement.setString(4,current.getResourcetype().getResourcetype()); 
                    System.out.println("save 3");
                    System.out.println("rc save " + getSelectedCategory().getCategoryID());
                    preparedstatement.setLong(5,getSelectedCategory().getCategoryID());
                    System.out.println("save 4");
                    preparedstatement.setString(6,current.getAuthorfirstname());
                    preparedstatement.setString(7,current.getAuthormiddlename());
                    preparedstatement.setString(8,current.getAuthorlastname());
                    preparedstatement.setString(9,current.getComment());
                    System.out.println("rc save stmt ="+ preparedstatement.toString() + preparedstatement.getGeneratedKeys());
                    numResourcesAdded  = preparedstatement.executeUpdate();  
                    if(numResourcesAdded == 1){
                        msg = new FacesMessage(numResourcesAdded + " resource added ");    
                    } else if (numResourcesAdded > 1) {
                        msg = new FacesMessage(numResourcesAdded + " resources added"); 
                    } else{    
                        msg = new FacesMessage("Unable to save the resource: ");
                    }
                    FacesContext.getCurrentInstance().addMessage("null", msg);
                    //if there are no errors redirect the form so that the user may immediately enter a new resource. NB There will be no success message with a redirect.
                    current.setResourceid(null);
                    current.setResourcename(null);
                    current.setIsbn(null);
                    setSelectedCategory(null);
                    current.setAuthorfirstname(null);
                    current.setAuthormiddlename(null);
                    current.setResourcetype(null);
                    current.setComment(null);
                    current.setAuthorlastname(null);
                    returnValue="addResource?faces-redirect=true";
                } else{
                    msg = new FacesMessage("This resource has not been properly returned. Please return it.");
                    FacesContext.getCurrentInstance().addMessage("null",msg);
                }
            } catch(SQLException ex){
                 msg = new FacesMessage("" + ex);
                FacesContext.getCurrentInstance().addMessage("null",msg);
                System.out.println("rc save ex " + ex);
                //msg = new FacesMessage("Unable to save the resource 2: " + ex);
                //FacesContext.getCurrentInstance().addMessage("j_idt6:addResourceForm:growl", msg); //set the client id
                //FacesContext.getCurrentInstance().addMessage("null", msg);   
            } catch(Exception e1){
                msg = new FacesMessage("rc save e1 " + e1);
                FacesContext.getCurrentInstance().addMessage("null",msg);
                System.out.println("rc save e1 "+ e1);
            }    
            /*} finally{
                try{
                    preparedstatement.close();
                } catch(SQLException ex){   
                    JsfUtil.addErrorMessage("Could not close the prepared statement in Resource Controller Save() " + ex);
                }
            }
        }*/
        return returnValue;//"addResource?faces-redirect=true"; NB messages are not displayed if the page is redirected
    }
    
    public String Clear(){ // Clears the form of a previous resource so a blank form is ready for adding a new resource.
        current.setResourceid(null);
        current.setResourcename(null);
        current.setIsbn(null);
        current.setCategory(null);
        current.setAuthorfirstname(null);
        current.setAuthormiddlename(null);
        current.setResourcetype(null);
        current.setComment(null);
        current.setAuthorlastname(null);
        return "addResource?faces-redirect=true";
    }            
        
    
    public void onRowEdit(RowEditEvent event) {
        Object obj = event.getObject();
        CLibResource resource = (CLibResource) obj;
        ejbFacade.edit(resource);
    }
    
    public void onRowSelect(SelectEvent event) {
        Object obj = event.getObject();
        CLibResource resource = (CLibResource) obj;
        ejbFacade.edit(resource);
    }
    
    public void onRowEditCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onCellEdit(CellEditEvent event) {       
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();       
        if(newValue != null && !newValue.equals(oldValue)) {          
            System.out.println("rc onCellEdit Changed rowkey=" + event.getRowKey() + " field=" + event.getColumn().getField() + " " + event.getNewValue().toString());
        }       
    }
    
    public String loadResources() {
        //ToDo ensure resources are selected by State="A" 
        resourcePageList.clear();
        //On a Windows PC:\Users\DL\workspace\clibrary3-db\src\main\java\org\clibrary\db\hibernatequeries
        resourcePageList.addAll(ejbFacade.findAll());
        return "";
    }

    public String loadLostResources() {
        resourcePageList.clear();
        resourcePageList.addAll(ejbFacade.findAllLost());
        return "";
    }
    
    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the current Resource
     */
    public CLibResource getCurrent() {
        return current;
    } //When adding a resource this is called second. It is called iteratively, each pass (at the end of the process) picking up a new value from the addResources.xhtml form

    /**
     * @param current the current Resource to set
     */
    public void setCurrent(CLibResource current) {
        this.current = current;
    }

    /**
     * @return the resourcePageList
     */
    public List<CLibResource> getResourcePageList() {
        return resourcePageList;
    }

    /**
     * @param resourcePageList the ResourceategoryPageList to set
     */
    public void setResourcePageList(List<CLibResource> resourcePageList) {
        this.resourcePageList = resourcePageList;
    }
    
     /**
     * @param ResourceTypesList get the ResourceTypesList 
     * @return  
     */
    public List<String> getResourceTypesList(){
        getResourceTypes();
        return StringList;
    }
    
    /**
     * getResourceTypes() populates a dropdown list of resource types for the user to select from.
     */
    
    private void getResourceTypes(){
        String SQLQuery = "SELECT * FROM public.\"ResourceType\" order by 1 desc";
        ResultSet resultset = null;
        Statement statement = null;
        try{
            Connection connection = DBConn.getDBConn();
            statement = connection.createStatement();
            resultset = statement.executeQuery(SQLQuery);
            StringList.clear();
            while(resultset.next()){
                StringList.add(resultset.getString(1));
            }     
            resultset.close();
            statement.close();
        } catch(SQLException ex){
            String msg = "";
                    Throwable cause = ex.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
        } finally{
            try{
                if( !resultset.isClosed()){
                    resultset.close();
                }
                if(!statement.isClosed()){
                    statement.close();
                }    
            }catch (SQLException sqle){
                String msg = "";
                Throwable cause = sqle.getCause();
                if (cause != null) {
                        msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                } else {
                        JsfUtil.addErrorMessage(sqle, ResourceBundle.getBundle("/Bundle").getString("Error closing SQL statements"));
                }
            }
        }
    }
    
    private CLibResource selectedCLibResource;
    public CLibResource getSelectedCLibResource(){
        return selectedCLibResource;
    }
    public void setSelectedCLibResource(CLibResource selectedCLibResource){
        this.selectedCLibResource = selectedCLibResource;
    }
    
    
    public Category getSelectedCategory(){
        return selectedCategory;
    }
    public void setSelectedCategory(Category selectedCategory){
        this.selectedCategory = selectedCategory;
    }
    
    public String lost(){
        getSelectedCLibResource().setState('L');
        ejbFacade.edit(getSelectedCLibResource());
        return "";
    }
    
     public String restore(){
        getSelectedCLibResource().setState('A');
        ejbFacade.edit(getSelectedCLibResource());
        return "";
    }
     
    public void VHS(){ //Deletes all VHS resources & associated loans 
        String SQLQuery = "Delete * FROM public.\"Loan\" WHERE \"Resource\".\"ResourceID\" in( " +
            " SELECT \"Resource\".\"ResourceID\" FROM public.\"Resource\" WHERE \"Resource\".\"ResourceType\" = 'VHS')";
        int numDeleted;
        ResultSet resultset = null;
        Statement statement = null;
        try{
            Connection connection = DBConn.getDBConn();
            statement = connection.createStatement();
            numDeleted = statement.executeUpdate(SQLQuery);  
            
            SQLQuery = " Delete * FROM public.\"Resource\" WHERE \"Resource\".\"ResourceType\" = 'VHS'";
            numDeleted = statement.executeUpdate(SQLQuery);
        } catch(SQLException ex){
            String msg = "";
                    Throwable cause = ex.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
        } finally{
            try{
                if( !resultset.isClosed()){
                    resultset.close();
                }
                if(!statement.isClosed()){
                    statement.close();
                }    
            }catch (SQLException sqle){
                String msg = "";
                Throwable cause = sqle.getCause();
                if (cause != null) {
                        msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                } else {
                        JsfUtil.addErrorMessage(sqle, ResourceBundle.getBundle("/Bundle").getString("Error closing SQL statements"));
                }
            }
        }
    } 
}
