/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import org.primefaces.event.ResizeEvent;

/**
 *
 * @author ascott
 */
@Named(value = "addResource")
@RequestScoped
public class AddResource {

//    @NotNull
    private String ResourceName;

    /**
     * Creates a new instance of AddResource
     */
    public AddResource() {
        System.out.println("ar ar");
    }

    public String save() {
        System.out.println("ar save");
        // Ensure that borrowers can be added quickly by resetting the form automatically.
        reset();
        return "addResource";
    }

    private void reset() {

        ResourceName = null;
    }

    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the ResourceName
     */
    public String getResourceName() {
        return ResourceName;
    }

    /**
     * @param ResourceName the resource name to set
     */
    public void setResourceName(String ResourceName) {
        System.out.println("ar set r name");
        this.ResourceName = ResourceName;
    }
}
