/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.model;

import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author ascott
 */
@Named
public class Borrower {
    
    private boolean selected;

    @NotNull
    //@Pattern(regexp="[\\w\\.-]*[a-zA-Z0-9_]")
    private String borrowerid;
    
    @NotNull
    @Pattern(regexp="[\\w\\.-]*[a-zA-Z_]")
    private String firstname;
    
    @NotNull
    @Pattern(regexp="[\\w\\.-]*[a-zA-Z_]")
    private String lastname;
    
    @NotNull
    @Pattern(regexp="[\\w\\.-]*[a-zA-Z0-9_]")
    private String address;
    @Pattern(regexp="[\\w\\.-]*[0-9_]")
    private String homePhone;
    @Pattern(regexp="[\\w\\.-]*[0-9_]")
    private String mobile;
    
    @Pattern(regexp="[\\w\\.-]*[a-zA-Z0-9_]@[\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]", message="Email '${validatedValue}' is invalid -- from backend.")
    private String email;
    @Pattern(regexp="[\\w\\.-]*[a-zA-Z0-9_]")
    private String comment;

    public void reset() {
        borrowerid = null;
        firstname = null;
        lastname = null;
        address = null;
        homePhone = null;
        mobile = null;
        email = null;
        comment = null;
    }

    /**
     * @return the Borrower ID
     */
    public String getBorrowerid() {
        return borrowerid;
    }

    /**
     * @param borrowerid the Borrower ID to set
     */
    public void setBorrowerid(String borrowerid) {
        this.firstname = borrowerid;
    }
    
    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the homePhone
     */
    public String getHomePhone() {
        return homePhone;
    }

    /**
     * @param homePhone the homePhone to set
     */
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
         System.out.println("model.Borrower.java setSelected() "+ selected);
        this.selected = selected;
    }

}
