/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import org.primefaces.event.ResizeEvent;

/**
 *
 * @author ascott
 */
@Named(value = "addCategory")
@RequestScoped
public class AddCategory {

//    @NotNull
    private int CategoryID;
    private String CategoryName;
    private String Code;

    /**
     * Creates a new instance of AddCategory
     */
    public AddCategory() {
    }

    public String save() {
        System.out.println("Saved " + CategoryName);
        // Ensure that borrowers can be added quickly by resetting the form automatically.
        reset();
        return "addCategory";
    }

    private void reset() {

        CategoryName = null;
    }

    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the CategoryName
     */
    public String getCategoryName() {
        return CategoryName;
    }

    /**
     * @param CategoryName the category name to set
     */
    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }
    
    /**
     * @return the CategoryName
     */
    public String getCategoryCode() {
        return Code;
    }

    /**
     * @param CategoryName the category name to set
     */
    public void setCategoryCode(String Code) {
        this.Code = Code;
    }
}
