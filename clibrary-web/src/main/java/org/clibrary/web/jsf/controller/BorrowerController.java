/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.controller;

import org.clibrary.db.hibernate.Borrower;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.clibrary.db.jsf.DBConn;
import org.clibrary.db.jsf.TwoStrings;
import org.clibrary.db.jsf.util.JsfUtil;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.ResizeEvent;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author ascott
 */
@Named(value = "myBorrowerController")
//@RequestScoped
@SessionScoped
//@ViewScoped
public class BorrowerController implements Serializable {
      
    static final long serialVersionUID = -1L;
	
    public List<TwoStrings.TwoStringsRecord> getList(){
        return GetReport();
    }
    
    public List<TwoStrings.TwoStringsRecord> GetReport() {
        //This is called multiple times!
        List<TwoStrings.TwoStringsRecord> report = new ArrayList<>();
        ResultSet resultset = null;
        Statement statement = null;
        String SQLQuery = null;
        TwoStrings.TwoStringsRecord twoStringsRecord = null;
        Connection connection = DBConn.getDBConn();
            try{
                //Get the Borrower ID passed in from Loan.html
                    statement = connection.createStatement();
                    SQLQuery="Select \"Resource\".\"ResourceName\", \"Loan\".\"Borrowed\" \n" +
                        "from \"Loan\", \"Resource\" \n" +
                        "where \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" "
                        + "AND \"Loan\".\"BorrowerID\" = '"+ current.getBorrowerid() +"'\n" +
                        "and \"Loan\".\"Returned\" is null order by 2";
                    resultset = statement.executeQuery(SQLQuery);
                    while(resultset.next()){
                        twoStringsRecord = new TwoStrings.TwoStringsRecord(resultset.getString(1).concat(", "), resultset.getString(2));
                        report.add(twoStringsRecord);
                    }       
            }catch(SQLException e1){
                String msg = "";
                Throwable cause = e1.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(e1, ResourceBundle.getBundle("/Bundle").getString("Error"));
                } 
            } catch (Exception e2){
                System.out.println("bc error: " + e2);
            } finally {
                try {
                    if(connection != null)
                        connection.close();
                    if(resultset != null)
                        resultset.close();
                    if(statement != null)
                        statement.close();
                } catch(SQLException ex){
                    String msg = "";
                    Throwable cause = ex.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
                }        
            }  
        return report;
    }
    
    @EJB
    private org.clibrary.db.bean.BorrowerFacade ejbFacade;
    
    private Borrower current = new Borrower();
    
    public String GetCurrent (){
        return current.toString();
    }
    
    private List<Borrower> borrowerPageList = new ArrayList<>();
//    private BorrowerDaoMock borrowerDaoMock = new BorrowerDaoMock();
    
    private static final boolean error = false;
    /**
     * Creates a new instance of AddBorrower
     */
    public BorrowerController() {
    }
    
    public String save() {
        try{
            ejbFacade.create(current);
            FacesMessage msg = new FacesMessage("Added new borrower "+ current.getFirstname() + " " + current.getLastname());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            borrowerPageList.removeAll(borrowerPageList); //untested
            current.setBorrowerid(null);
            current.setAddress(null);
            current.setComment(null);
            current.setEmail(null);
            current.setFirstname(null);
            current.setLastname(null);
            current.setPhone(null);
            current.setMobile(null);
            current.setComment(null);          
        } catch (Exception e){
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error - reason: " + e.toString(), "long");
            FacesContext.getCurrentInstance().addMessage(null, facesMsg);
        }
        return "";
    }
    
    /*public String search() {
        FacesMessage msg;
        try{
            if(current.getFirstname().length() != 0){
                DoSearch(current.getFirstname(),1);
            }else if(current.getLastname() != null){
                DoSearch(current.getLastname(),2);
            }    
            int numFound = borrowerPageList.size();
            switch (numFound) {
                case 0:
                    msg = new FacesMessage("No matching borrowers found.");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    break;        
                default:
                    msg = new FacesMessage( numFound + " matching borrowers found.");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    break;
            }
            msg = new FacesMessage("Searching");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (NullPointerException npe){
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, npe.toString(), "long");
            FacesContext.getCurrentInstance().addMessage(null, facesMsg);
        }
        return "filterBorrowers?faces-redirect=true";
    }*/
    
    public String resetPage(){
        borrowerPageList.removeAll(borrowerPageList); //untested
        current.setBorrowerid(null);
        current.setAddress(null);
        current.setComment(null);
        current.setEmail(null);
        current.setFirstname(null);
        current.setLastname(null);
        current.setPhone(null);
        current.setMobile(null);
        current.setComment(null);
        return "" ; //findPerson?faces-redirect=true";
    }    
    
    public String resetAddBorrowerPage(){
        borrowerPageList.removeAll(borrowerPageList); //untested
        current.setBorrowerid(null);
        current.setAddress(null);
        current.setComment(null);
        current.setEmail(null);
        current.setFirstname(null);
        current.setLastname(null);
        current.setPhone(null);
        current.setMobile(null);
        current.setComment(null);
        return "addBorrower?faces-redirect=true";
    }    
//    public String save2() {
//
//        RequestContext requestContext = RequestContext.getCurrentInstance();
//        if (error) {
//            error = false;
//            String shortMsg = "Error in adding " + current.getFirstname() + " " + current.getLastname() + " at " + current.getAddress() + ": Database not available.";
//            String longMsg = "Error in adding " + current.getFirstname() + " " + current.getLastname() + ": Database not available.";
////            requestContext.addCallbackParam("age", true);
//            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, shortMsg, longMsg);
//            FacesContext.getCurrentInstance().addMessage(null, facesMsg);
//            
//            return "";
//        } else {
//            error = true;
//            String shortMsg = "Successfully added " + current.getFirstname() + " " + current.getLastname() + " at " + current.getAddress();
//            String longMsg = "Successfully added " + current.getFirstname() + " " + current.getLastname() + " at " + current.getAddress();
////            requestContext.addCallbackParam("age", false);
//            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, shortMsg, longMsg);
//            FacesContext.getCurrentInstance().addMessage(null, facesMsg);
//
//            // Ensure that borrowers can be added quickly by resetting the form automatically.
//            current.reset();
//            return "addBorrower";
//        }
//    }
    
    public void onCellEdit(CellEditEvent event) {       
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();       
        if(newValue != null && !newValue.equals(oldValue)) {          
            System.out.println("bc onCellEdit Changed rowkey=" + event.getRowKey() + " field=" + event.getColumn().getField() + " " + event.getNewValue().toString());
        }       
        System.out.println(event.getClass());
    }
    
    public void onRowEdit(RowEditEvent event) {
        Object obj = event.getObject();
        Borrower borrower = (Borrower) obj;
        ejbFacade.edit(borrower);
        System.out.println("bc onRowEdit Changed");
        
        
        
//        if(StringUtils.isBlank(borrower.getAddress())) {
//        }
//        FacesMessage msg = new FacesMessage("Car Edited");
//        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onRowEditCancel(RowEditEvent event) {

        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public String loadBorrowers() {
        
        borrowerPageList.clear();
        //On a Windows PC:\Users\DL\workspace\clibrary3-db\src\main\java\org\clibrary\db\hibernatequeries
        borrowerPageList.addAll(ejbFacade.findAll());
        //int[] range = {3,8,9,13};
        //borrowerPageList.addAll(ejbFacade.findRange(range));
        return "";
    }

    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the current
     */
    public Borrower getCurrent() {
        return current;
    }

    /**
     * @param current the current to set
     */
    public void setCurrent(Borrower current) {
        this.current = current;
    }

    /**
     * @return the borrowerPageList
     */
    public List<Borrower> getBorrowerPageList() {
        return borrowerPageList;
    }

    /**
     * @param borrowerPageList the borrowerPageList to set
     */
    public void setBorrowerPageList(List<Borrower> borrowerPageList) {
        this.borrowerPageList = borrowerPageList;
    }  
    
    public String Help(){
        return "personalHelp";
    }
    
}
