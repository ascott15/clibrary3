/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.model;

import javax.inject.Named;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ascott
 */
@Named
public class Category {
    
    private boolean selected;

    @NotNull
    private String CategoryName ;
    private Long CategoryID;
    private String CategoryCode;

    public void reset() {
        CategoryName  = null;
    }

    public Long getCategoryID () {
        return CategoryID ;
    }

    /**
     * @param CategoryID  the CategoryID to set
     */
    public void setCategoryID (Long CategoryID) {
        this.CategoryID  = CategoryID ;
    }
    
    public String getCategoryName () {
        return CategoryName ;
    }

    /**
     * @param CategoryName  the CategoryName to set
     */
    public void setCategoryName (String CategoryName) {
        this.CategoryName  = CategoryName ;
    }

    public String getCategoryCode () {
        return CategoryCode ;
    }

    /**
     * @param CategoryName  the CategoryName to set
     */
    public void setCategoryCode (String CategoryCode) {
        this.CategoryCode  = CategoryCode ;
    }
    
    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
