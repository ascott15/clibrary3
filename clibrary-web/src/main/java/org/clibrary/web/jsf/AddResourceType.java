/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import org.primefaces.event.ResizeEvent;

/**
 *
 * @author ascott
 */
@Named(value = "addResourceType")
@RequestScoped
public class AddResourceType {

//    @NotNull
    private String ResourceType;

    /**
     * Creates a new instance of AddResourceType
     */
    public AddResourceType() {
    }

    public String save() {
        System.out.println("Saved " + ResourceType);
        // Ensure that borrowers can be added quickly by resetting the form automatically.
        reset();
        return "addResourceType";
    }

    private void reset() {

        ResourceType = null;
    }

    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the ResourceType
     */
    public String getResourceType() {
        return ResourceType;
    }

    /**
     * @param ResourceType the ResourceType to set
     */
    public void setResourceType(String ResourceType) {
        this.ResourceType = ResourceType;
    }
}
