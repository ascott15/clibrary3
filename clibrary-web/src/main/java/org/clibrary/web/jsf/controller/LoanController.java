/*--
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.clibrary.db.hibernate.Borrower;
import org.clibrary.db.hibernate.Loan;
import org.clibrary.db.jsf.DBConn;
import org.clibrary.db.jsf.ThreeStrings;
import org.clibrary.db.jsf.TwoStrings;
import org.clibrary.db.jsf.util.JsfUtil;
import org.primefaces.event.ResizeEvent;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author ascott
 */
@Named(value = "myLoanController")
@SessionScoped
public class LoanController implements Serializable {
 
    @EJB
    private org.clibrary.db.bean.LoanFacade ejbFacade; 
    static final long serialVersionUID = -1L;
    private Loan current = new Loan();
    private Loan newCurrent = new Loan();
    private final Borrower borrower= new Borrower();
    private List<Loan> loanPageList = new ArrayList<>();
    private static final boolean error = false;
    
    private Borrower selectedBorrower;
    public Borrower getSelectedBorrower(){
        return selectedBorrower;
    }
    public void setSelectedBorrower(Borrower selectedBorrower){
        this.selectedBorrower = selectedBorrower;
    }
    
    public String getFilterResult(){  
        return "loan";
    }
    
    public List<TwoStrings.TwoStringsRecord> getList(){
        return getReport();
    } 
   
    public String returnItem(){ //returns ALL instances of this resource which are out - even if it has just been borrowed again before the return is registered! keep it so because the resource's return must have been overlooked?
        int result;
	PreparedStatement statement = null;
	Connection connection = null;  
        FacesMessage msg= null;
        try {
            connection = DBConn.getDBConn();
            Date today = new Date(System.currentTimeMillis());
            statement = connection.prepareStatement("UPDATE public.\"Loan\" " +
                "SET \"Returned\" = ? " +
                "WHERE \"Loan\".\"ResourceID\" = ?" +
                "and \"Loan\".\"Returned\" is null"); 
            statement.setDate(1,today);
            statement.setString(2,current.getResource().getResourceid());
            result = statement.executeUpdate();
            if(result == 1){
                msg = new FacesMessage(result + " item )" +current.getResource().getResourceid() + ") returned" );
            } else if(result > 1){
                msg = new FacesMessage(result + " items (" +current.getResource().getResourceid() + ") returned. Was the resource not returned before being loaned out again?" );
            } else{
                msg = new FacesMessage("There is no message");
            }   
            FacesContext.getCurrentInstance().addMessage(null, msg);
           
            current.setResource(null);
	} catch (SQLException e) {
            msg = new FacesMessage("Loan return error: " + e);
            FacesContext.getCurrentInstance().addMessage(null, msg); 
	} finally{
            try {
                if(connection != null)
                   connection.close();
                if(statement != null)
                        statement.close();
            } catch (SQLException e) {
                msg = new FacesMessage("Loan return error: " + e);
                FacesContext.getCurrentInstance().addMessage(null, msg); 
            }
        }	
        //return "return?faces-redirect=true";    
        return "";
    }
    
    public List<TwoStrings.TwoStringsRecord> getReport(){
        int counter = 0;
        ResultSet resultset = null;
        PreparedStatement statement = null;
        Connection connection = DBConn.getDBConn();
        TwoStrings.TwoStringsRecord twoStringsRecord = null;
        List<TwoStrings.TwoStringsRecord> report = new ArrayList<>();
            try{
                if(selectedBorrower.getBorrowerid() != null){
                    statement = connection.prepareStatement("Select \"Resource\".\"ResourceName\", to_char(\"Loan\".\"Borrowed\",'DD Mon YYYY') " +
                        "from \"Loan\", \"Resource\" " +
                        "where \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" "
                        + "AND \"Loan\".\"BorrowerID\" = ?"
                        +" and \"Loan\".\"Returned\" is null order by 2 desc");
                    statement.setString(1,selectedBorrower.getBorrowerid());        
                    resultset = statement.executeQuery();
                    while(resultset.next()){
                        twoStringsRecord = new TwoStrings.TwoStringsRecord(resultset.getString(1).concat(" "), resultset.getString(2));
                        report.add(twoStringsRecord);
                        counter ++;
                    }
                    //personalLoansTitle = " for ".concat(selectedBorrower.getFirstname()).concat(" ").concat(selectedBorrower.getLastname().concat(":")); this can return the name of the last borrower
                } else {
                    personalLoansTitle = "";
                    BorrowerLoans = "Could not check if "+ selectedBorrower.getFirstname() + " " + selectedBorrower.getLastname() + " has loans.";
                }
                if(counter == 0){
                    personalLoansTitle= ""; //As there are no loans a title is not needed.
                }
            }catch(SQLException e1){
                String msg = "";
                Throwable cause = e1.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(e1, ResourceBundle.getBundle("/Bundle").getString("Error"));
                }    
            }catch(Exception e2){
                String msg = "";
                Throwable cause = e2.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(e2, ResourceBundle.getBundle("/Bundle").getString("Error"));
                }        
            } finally {
                try {
                    if(connection != null)
                        connection.close();
                    if(resultset != null)
                        resultset.close();
                    if(statement != null)
                        statement.close();
                } catch(SQLException ex){
                    String msg = "";
                    Throwable cause = ex.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
                }        
            }  
        return report;
    } //Show the loans the borrower has out after every resource is checked out. As it's not essential to the procesd errors can simply be logged.
    
    private String BorrowerLoans = "None";
    public String getBorrowerLoans(){
        return BorrowerLoans;
    }  
    
    private String personalLoansTitle = "Current Loans";
    public String getPersonalLoansTitle(){
        return personalLoansTitle;
    }  
    
    //public LoanController() {
    //}
    
    List<ThreeStrings.ThreeStringsRecord> surnameLoansList = new ArrayList<>();
    public List<ThreeStrings.ThreeStringsRecord> getSurnameLoansList(){
        return surnameLoansList;
    }
    
    public String surnameLoansList(){ //Populates the surnameLoansList
        int counter = 0;
        ResultSet resultset = null;
        PreparedStatement statement = null;
        String firstname = " ";
        surnameLoansList.clear();
        Connection connection = DBConn.getDBConn();
        ThreeStrings.ThreeStringsRecord threeStringsRecord = null;
            try{
                if(selectedBorrower.getBorrowerid() != null){
                        String SQL= "Select \"Borrower\".\"FirstName\","
                        + "\"Borrower\".\"LastName\", \"Resource\".\"ResourceName\", "
                        + "\"Category\".\"CategoryName\" , to_char(\"Loan\".\"Borrowed\",'DD Mon YYYY') "
                        + "from \"Loan\", \"Resource\", \"Borrower\", \"Category\" "
                        + "where \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" "
                        + "and \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" "    
                        + "and \"Loan\".\"Returned\" is null "
                        + "and \"Category\".\"CategoryID\" = \"Resource\".\"CategoryID\" "    
                        + "AND \"Resource\".\"State\" = 'A' "
                        + "AND upper(\"Borrower\".\"LastName\") = "
                        + "(Select UPPER(\"Borrower\".\"LastName\") "
                        + "FROM \"Borrower\" where \"Borrower\".\"BorrowerID\" = ?) "    
                        + "order by 1, 2 desc";
                    statement = connection.prepareStatement(SQL);    
                    System.out.println(selectedBorrower.getBorrowerid() + " " + SQL);
                    statement.setString(1,selectedBorrower.getBorrowerid()); 
                    resultset = statement.executeQuery();
                    while(resultset.next()){
                        if(!firstname.equalsIgnoreCase(resultset.getString(1))){ //Separate different borrowers
                            if(counter != 0){
                                threeStringsRecord = new ThreeStrings.ThreeStringsRecord("-","-","-");
                                surnameLoansList.add(threeStringsRecord);
                            } 
                            counter++;
                        } 
                        threeStringsRecord = new ThreeStrings.ThreeStringsRecord(resultset.getString(1).concat(" ").concat(resultset.getString(2)), resultset.getString(3).concat(" (").concat(resultset.getString(4)).concat(")"), resultset.getString(5));
                        surnameLoansList.add(threeStringsRecord);
                        firstname = resultset.getString(1);      
                    }
                }   
            }catch(SQLException e1){
                System.out.println("surnameLoansList e1 "+ e1);
                String msg = "";
                Throwable cause = e1.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(e1, ResourceBundle.getBundle("/Bundle").getString("Error"));
                }   
            }catch(Exception e2){
                System.out.println("surnameLoansList e2 "+ e2);
                String msg = "";
                Throwable cause = e2.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(e2, ResourceBundle.getBundle("/Bundle").getString("Error"));
                }       
            } finally {
                try {
                    if(connection != null)
                        connection.close();
                    if(resultset != null)
                        resultset.close();
                    if(statement != null)
                        statement.close();
                } catch(SQLException ex){
                    String msg = "";
                    Throwable cause = ex.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
                }        
            }  
            return "SurnameLoansReport";
    } //Show the loans the borrower has out after every resource is checked out.
    
    /**
     * Saves a new Loan
     * @return a string
     */   
    
    public String save() {
        boolean resourceNotReturned = CheckReturned(current.getResource().getResourceid());
        String returnValue = "loan?faces-redirect=true";
        try{ 
            if(resourceNotReturned == false){
            //Check the item has been returned before loaning it.
            Calendar calendar = Calendar.getInstance();
            borrower.setBorrowerid(selectedBorrower.getBorrowerid());
            newCurrent.setBorrower(borrower);
            newCurrent.setBorrowed(calendar.getTime());
            newCurrent.setResource(current.getResource());
            newCurrent.setComment(current.getComment());
            ejbFacade.create(newCurrent); //Loan.java called ere this call
            newCurrent = new Loan();
            } else {
                System.out.println("Please return this resource before loaning it out"); 
                FacesMessage msg = new FacesMessage("Please return this resource before loaning it out");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                returnValue = "";
            }
        } catch (Exception e){
            System.out.println("lc save() error: " + e); 
            FacesMessage msg = new FacesMessage("Error creating the loan: " + e);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        return returnValue;
    }
    
    public boolean CheckReturned(String resourceID){
        boolean resourceNotReturned = false;
        ResultSet resultset = null;
        PreparedStatement statement = null;
        Connection connection = DBConn.getDBConn();
        int numTimesNotReturned = 0;
        try{
            statement = connection.prepareStatement("SELECT count(*) " +
                "FROM public.\"Loan\" WHERE \"Loan\".\"ResourceID\" = ? " +
                "AND \"Loan\".\"Returned\" is null");  
            statement.setString(1,resourceID);
            resultset = statement.executeQuery();
            while(resultset.next()){
                numTimesNotReturned = numTimesNotReturned + resultset.getInt(1); //The total number of overdue items.
            }
            if(numTimesNotReturned == 1){
                resourceNotReturned = true;
            }    
        }catch(SQLException sqle){
            sqle.getErrorCode();
            FacesMessage msg = new FacesMessage("SQL Could not check resource not returned: " + sqle.toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }catch(Exception e){
            FacesMessage msg = new FacesMessage("E Could not check resource not returned: " + e.toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } finally{
            try{
                if(!statement.isClosed()){
                    statement.close();
                }
                if(!connection.isClosed()){
                    connection.close();
                }
                if(resultset.isClosed()){
                } else {
                    resultset.close();
                }
            } catch (SQLException | NullPointerException sqlEx){
                FacesMessage msg = new FacesMessage("Error finishing the check for overdue loans" + sqlEx.toString());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }    
        }    
        return resourceNotReturned;
    }  
    
    public void onRowEdit(RowEditEvent event) {
        Object obj = event.getObject();
        Loan loan = (Loan) obj;
        ejbFacade.edit(loan);
    }
    
    public void onRowEditCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public String loadCategories() {
        loanPageList.clear();
        //On a Windows PC:\Users\DL\workspace\clibrary3-db\src\main\java\org\clibrary\db\hibernatequeries
        loanPageList.addAll(ejbFacade.findAll());
        return "";
    }

    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the current loan
     */
    public Loan getCurrent() {
        return current;
    }

    /**
     * @param current the current loan to set
     */
    public void setCurrent(Loan current) {
        this.current = current;
    }

    /**
     * @return the loanPageList
     */
    public List<Loan> getLoanLoanPageList() {
        return loanPageList;
    }

    /**
     * @param loanPageList the loanPageList to set
     */
    public void setLoanPageList(List<Loan> loanPageList) {
        this.loanPageList = loanPageList;
    }
    
    public String getBorrowerName(){ //replace this with a paramter from the findPerson form
        String name=null;
        try{
            //FacesContext facesContext = FacesContext.getCurrentInstance();
            //Map<String, String> parameter = facesContext.getExternalContext().getRequestParameterMap();
            name = "New loan for ".concat(selectedBorrower.getFirstname().concat(" ").concat(selectedBorrower.getLastname()));
        }catch(NullPointerException e){  
            FacesMessage msg = new FacesMessage("Error getting the persons name: " + e);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        return name;
    }
    
    private String BorrowerLoansMessages;
    public String getBorrowerLoansMessages(){
        checkForOverdueLoans();
        return BorrowerLoansMessages;
    }
    public String setBorrowerLoansMessages(String BorrowerLoansMessages){
        this.BorrowerLoansMessages = BorrowerLoansMessages;
        return BorrowerLoansMessages;
    }
    
    /**
     * Called by clicking on the loan button on the findPerson.xhtml file. It checks if the person whose details are being displayed has overdue loans and how many loans people with the same surname have out. It would be good if the overdue loans were shown in red.
     */
    public String checkForOverdueLoans(){  
        //Make it a prepared statement
        ResultSet resultset = null;
        PreparedStatement statement = null;
        Connection connection = null;
        int numOverdueLoans=0;
        int numFamilyLoans = 0;  
        //Is the returned value null for Loan.Returned in the production database?
        try{
            connection = DBConn.getDBConn();
            //Check for CDs and DVDs
            statement = connection.prepareStatement("SELECT count(*) " +
            "FROM public.\"Loan\", public.\"Borrower\", public.\"Resource\"" +
            "where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" " +
            "and \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" " +
            "and \"Loan\".\"Returned\" is null " +
            "AND \"Resource\".\"State\" = 'A' " +        
            "and age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) > '14 days'::interval " +
            "AND (\"Resource\".\"ResourceType\" = 'CD' OR \"Resource\".\"ResourceType\" = 'DVD') " +
            "AND \"Borrower\".\"BorrowerID\" = ?");
            statement.setString(1, selectedBorrower.getBorrowerid());        
            resultset = statement.executeQuery();
            while(resultset.next()){
                numOverdueLoans = resultset.getInt(1); //The number of CDs or DVDs overdue
            }
            //Check the number of loans people with the same surname as the selected borrower have out. This enables users to see if a family has too many items out.
            statement = connection.prepareStatement("select count(*) " +
                "from \"Loan\", \"Borrower\" " +
                "where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" " +
                "and \"Loan\".\"Returned\" is null " +
                "AND \"Resource\".\"State\" = 'A' " +   
                "AND \"Borrower\".\"LastName\" = ?");
            statement.setString(1,selectedBorrower.getLastname());
            resultset = statement.executeQuery();
            while(resultset.next()){
                numFamilyLoans = resultset.getInt(1);  
            } 
            if( numFamilyLoans> 4 ){ //Put the maximum number of loans in a constant somewhere?
                //make the "Search by Surname" button visible
                BorrowerLoansMessages= "People surnamed " + selectedBorrower.getLastname() + " have " + numFamilyLoans + " loans out. Use the 'Search by Surname' button to see details."; //The total number of items out by people with the current borrower's surname.
            }
            //Checks for items other than CDs and DVDs as they have different loan periods
            statement = connection.prepareStatement("SELECT count(*) " +
                "FROM public.\"Loan\", public.\"Borrower\", public.\"Resource\" " +
                "where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" " +
                "and \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" " +
                "and \"Loan\".\"Returned\" is null " +
                "AND \"Resource\".\"State\" = 'A' " +
                "and age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) > '28 days'::interval " +
                "AND \"Borrower\".\"BorrowerID\" = ? " +
                "AND (\"Resource\".\"ResourceType\" != 'CD' AND \"Resource\".\"ResourceType\" != 'DVD')");  
            statement.setString(1,selectedBorrower.getBorrowerid());
            resultset = statement.executeQuery();
            while(resultset.next()){
                numOverdueLoans = numOverdueLoans + resultset.getInt(1); //The total number of overdue items.
            }
            if(numOverdueLoans == 1){
                BorrowerLoansMessages = "NB ".concat(selectedBorrower.getFirstname()).concat(" ").concat(selectedBorrower.getLastname()).concat(" has ").concat(" 1 overdue loan.");
                 System.out.println("num 1a " + BorrowerLoansMessages);
                FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,"", "long");
                FacesContext.getCurrentInstance().addMessage(null, facesMsg);    
            } else if (numOverdueLoans > 1){
                BorrowerLoansMessages = BorrowerLoansMessages.concat("NB ").concat(selectedBorrower.getFirstname()).concat(" ").concat(selectedBorrower.getLastname()).concat(" has ").concat(Integer.toString(numOverdueLoans)).concat(" overdue loans.");
                FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "This person has " + numOverdueLoans + " overdue loans.", "long");
                FacesContext.getCurrentInstance().addMessage(null, facesMsg);    
            }
        }catch(SQLException sqle){
            sqle.getErrorCode();
            FacesMessage msg = new FacesMessage("Could not check for overdue loans: " + sqle.toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }catch(Exception e){
            FacesMessage msg = new FacesMessage("Could not check for overdue loans: " + e.toString());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } finally{
            try{
                if(!statement.isClosed()){
                    statement.close();
                }
                if(!connection.isClosed()){
                    connection.close();
                }
                if(resultset.isClosed()){
                } else {
                    resultset.close();
                }
            } catch (SQLException | NullPointerException sqlEx){
                FacesMessage msg = new FacesMessage("Error finishing the check for overdue loans" + sqlEx.toString());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }    
        }    
        return "loan";
    }  
     
    /**
     * Called by filterBorrowers.xhtml. It deletes the borrower selected by the user if that borrower has never borrowed.
     */   
    public String Delete(){ //in the Loan cotroller file because this is where the selected borrowers details arrive          
            //ejbBorrowerFacade.remove(selectedBorrower);
            PreparedStatement statement = null;
            Connection connection = null; 
            FacesMessage facesMsg;
        try{
            int result;
            connection = DBConn.getDBConn();
            statement = connection.prepareStatement("Delete from public.\"Borrower\" WHERE \"Borrower\".\"BorrowerID\" = ?");
            statement.setString(1, selectedBorrower.getBorrowerid());        
            result = statement.executeUpdate();
            if(result == 0){
                facesMsg = new FacesMessage( result + " Borrowers deleted");
            } else if (result == 1){
                facesMsg = new FacesMessage( result + " Borrower deleted");
            } else{
                facesMsg = new FacesMessage( result + " Borrowers deleted");
            }
            FacesContext.getCurrentInstance().addMessage(null, facesMsg);    
        }catch(SQLException e){
            facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error - reason: " + e.toString(), "long");
            FacesContext.getCurrentInstance().addMessage(null, facesMsg);
        } finally{
            try{
                if(!statement.isClosed()){
                    statement.close();
                }
                if(!connection.isClosed()){
                    connection.close();
                }
            } catch (SQLException | NullPointerException sqlEx){
                FacesMessage msg = new FacesMessage("Error: " + sqlEx.toString());
                FacesContext.getCurrentInstance().addMessage(null, msg);
             } catch (Exception e1){
                FacesMessage msg = new FacesMessage("Error: " + e1.toString());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }    
        return ""; 
        }
    } 
}
