/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.web.jsf;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import org.primefaces.event.ResizeEvent;

/**
 *
 * @author ascott
 */
@Named(value = "addLoan")
@RequestScoped
public class AddLoan {

//    @NotNull
    private String LoanName;

    /**
     * Creates a new instance of AddLoan
     */
    public AddLoan() {
    }

    public String save() {
        System.out.println("Saved " + LoanName);
        // Ensure that borrowers can be added quickly by resetting the form automatically.
        reset();
        return "addLoan";
    }

    private void reset() {

        LoanName = null;
    }

    public void layoutResizeEvent(ResizeEvent event) {
        System.out.println("The layout has been sized " + event.getComponent());
    }

    /**
     * @return the LoanName
     */
    public String geLoanName() {
        return LoanName;
    }

    /**
     * @param LoanName the loan name to set
     */
    public void setLoanName(String LoanName) {
        this.LoanName = LoanName;
    }
}
