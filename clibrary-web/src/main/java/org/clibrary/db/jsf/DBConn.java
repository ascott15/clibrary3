package org.clibrary.db.jsf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConn {
	public static Connection connection = null;
	public static synchronized Connection getDBConn() {
		Properties properties = new Properties();
		properties.setProperty("user", "postgres");  //$NON-NLS-1$ //$NON-NLS-2$
		properties.setProperty("password",  "clibrary");  //$NON-NLS-1$ //$NON-NLS-2$
		properties.setProperty("ssl", "false");  //$NON-NLS-1$ //$NON-NLS-2$
		try {
			Class.forName("org.postgresql.Driver"); //$NON-NLS-1$
		} catch (ClassNotFoundException e4) {
			e4.printStackTrace();
			System.out.println("no class"); //$NON-NLS-1$
		}
		try {
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:2345/clibrary", properties); //$NON-NLS-1$
		}catch(SQLException e){
			System.out.println(e);
		} catch(NullPointerException e1){
                    System.out.println(e1);
                }
		return connection;
	}

}
