package org.clibrary.db.jsf;

import org.clibrary.db.hibernate.Category;
import org.clibrary.db.jsf.util.JsfUtil;
import org.clibrary.db.jsf.util.JsfUtil.PersistAction;
import org.clibrary.db.bean.CategoryFacade;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("categoryController")
@SessionScoped
public class CategoryController implements Serializable {

    @EJB
    private CategoryFacade ejbFacade;
    private List<Category> items = null;
    private Category selected;

    public CategoryController() {
    }

    public Category getSelected() {
        return selected;
    }

    public void setSelected(Category selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private CategoryFacade getFacade() {
        return ejbFacade;
//        return null;
    }

    public Category prepareCreate() {
        selected = new Category();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("CategoryCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("CategoryUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("CategoryDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Category> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Category getCategory(String id) {
        PreparedStatement preparedstatement = null;
        long retrievedID = 0;
        ResultSet resultSet;
        try{
            Connection connection = DBConn.getDBConn();
            //Check if this resource has not been returned before being presented as a new loan
            //Could this be picked up when an Overdue Loan report is produced and a borrower claims they've already returned the resource? Maybe a query to check the entire loan history for that resource would expose the problem.
            preparedstatement = connection.prepareStatement("select \"CategoryID\" from public.\"Category\" WHERE \"Category\".\"CategoryName\" = ?");
            preparedstatement.setString(1,id);
            resultSet = preparedstatement.executeQuery();
            while(resultSet.next()){
                retrievedID= resultSet.getInt(1);
            }
        } catch(SQLException sqle){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, sqle);
            JsfUtil.addErrorMessage(sqle, ResourceBundle.getBundle("/Bundle").getString("CategoryController.java getCategory() error"));
System.out.println("cc getCategory "+ sqle);
        }        
        return getFacade().find(retrievedID);
    }

    public List<Category> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Category> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Category.class)
    public static class CategoryControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            CategoryController controller = null;
            Object o = null; 
            try{
                if (value == null || value.length() == 0) {
                    return null;
                }
                controller = (CategoryController) facesContext.getApplication().getELResolver().
                        getValue(facesContext.getELContext(), null, "categoryController");
                o = controller.getCategory(getKey(value));
            } catch (NullPointerException npe){
                String msg = "cat controller msg";
                Throwable cause = npe.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(npe, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception e){
                FacesMessage msg = new FacesMessage("Category Controller error: " + e);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            return o;
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            //need to convert for passing as an http param
            if (object == null) {
                return null;
            }
            if (object instanceof Category) {
                Long catID = Long.valueOf(((Category) object).getCategoryID());
                return String.valueOf(catID);
            } else {
                System.out.println("cc getasstring else : wrong object");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Category.class.getName()});
                return null;
            }
        }
    }
}
