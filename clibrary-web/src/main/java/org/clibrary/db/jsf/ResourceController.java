package org.clibrary.db.jsf;

import org.clibrary.db.hibernate.CLibResource;
import org.clibrary.db.jsf.util.JsfUtil;
import org.clibrary.db.jsf.util.JsfUtil.PersistAction;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.clibrary.db.bean.ResourceFacade;

@Named("resourceController")
@SessionScoped
public class ResourceController implements Serializable {

    @EJB
    private ResourceFacade ejbFacade;
    private List<CLibResource> items = null;
    private CLibResource selected;

    public ResourceController() {
    }

    public CLibResource getSelected() {
        return selected;
    }

    public void setSelected(CLibResource selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ResourceFacade getFacade() {
        return ejbFacade;
//        return null;
    }

    public CLibResource prepareCreate() {
        selected = new CLibResource();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ResourceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ResourceUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ResourceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<CLibResource> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public CLibResource getResource(java.lang.String id) {
        return getFacade().find(id);
    }

    public List<CLibResource> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<CLibResource> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = CLibResource.class)
    public static class ResourceControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            ResourceController controller = null;
            Object obj = null;
            try{
                if (value == null || value.length() == 0) {
                    return null;
                }
                controller = (ResourceController) facesContext.getApplication().getELResolver().
                        getValue(facesContext.getELContext(), null, "resourceController");
                obj=controller.getResource(getKey(value));
            }catch(Exception e){
                FacesMessage msg = new FacesMessage("ResourceController error: " + e);
                FacesContext.getCurrentInstance().addMessage(null, msg); 
            }
            return obj;
        } //AbstractFacade called ere this function

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CLibResource) {
                CLibResource o = (CLibResource) object;
                return getStringKey(o.getResourcename());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), CLibResource.class.getName()});
                return null;
            }
        }
    }
}