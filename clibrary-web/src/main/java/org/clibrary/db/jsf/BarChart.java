package org.clibrary.db.jsf;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import org.clibrary.db.jsf.util.JsfUtil;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LegendPlacement;

@ManagedBean
@ViewScoped
public class BarChart implements Serializable{
    BarChartModel bcm = new BarChartModel();
    ChartSeries out = new ChartSeries();
    ChartSeries in = new ChartSeries();
    private Map<String, Number> chartData = new LinkedHashMap<>();
    private static final long serialVersionUID =1L;

    @PostConstruct
    public void init(){
        try{
            setChartData(chartData);
            bcm.setTitle("Loans & Returns for this Year");
            bcm.setLegendPosition("e");
            bcm.setLegendPlacement(LegendPlacement.OUTSIDE);

            Axis xAxis = bcm.getAxis(AxisType.X);
            xAxis.setLabel("Month");
            xAxis.setMin("1");
            xAxis.setMax("12");

            Axis yAxis = bcm.getAxis(AxisType.Y);
            yAxis.setLabel("Transactions");   
        } catch(SQLException ex){
            String msg = "";
            Throwable cause = ex.getCause();
            if (cause != null) {
                        msg = cause.getLocalizedMessage();
            }
            if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
            }
        }
    }
    
    public BarChartModel getBcm(){
        return bcm;
    }
    public void setChartData(Map<String, Number> chartData) throws SQLException{
        String SQLQuery = "SELECT date_part('month', \"Loan\".\"Borrowed\"), count(*) FROM public.\"Loan\" WHERE date_trunc('year', \"Loan\".\"Borrowed\") = date_trunc('year', now()) group by date_part('month', \"Loan\".\"Borrowed\") order by 1 desc";
        ResultSet resultset = null;
        Statement statement = null;
        try{
            Connection connection = DBConn.getDBConn();
            statement = connection.createStatement();
            resultset = statement.executeQuery(SQLQuery);
            in.setLabel("Returns");
                while(resultset.next()){
                    chartData.put(resultset.getString(1), resultset.getInt(2));
                    out.set(resultset.getString(1), resultset.getInt(2));
                }   
                bcm.addSeries(out);
            resultset.close();
            
            out.setLabel("Loans");
            SQLQuery = "SELECT date_part('month', \"Loan\".\"Returned\"), count(*) FROM public.\"Loan\" WHERE date_trunc('year', \"Loan\".\"Returned\") = date_trunc('year', now()) group by date_part('month', \"Loan\".\"Returned\") order by 1 desc";
            resultset = statement.executeQuery(SQLQuery);
            while(resultset.next()){
                    in.set(resultset.getString(1), resultset.getInt(2));
                }             
                bcm.addSeries(in);
            this.chartData = chartData;
        } catch(SQLException ex){
                String msg = "";
                    Throwable cause = ex.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
        } catch(Exception e){
            String msg = "";
                    Throwable cause = e.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
        } finally{
            if(resultset.isClosed()){
            } else {
                resultset.close();
            }
            statement.close();
        }
    }
}
