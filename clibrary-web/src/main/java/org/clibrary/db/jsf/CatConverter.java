/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.db.jsf;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.clibrary.db.hibernate.Category;

/**
 *
 * @author DL
 */
public class CatConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        System.out.println("cat conv getAsObject " + value.length());
        Category category = new Category();
        category.setCategoryID(1);
        try{
            
        }catch(Exception e){
            System.out.println("catc getAsObject err "+ e);
        }
        return category;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Category cat = new Category();    
        try{
            System.out.println("cat c getAsString "+ value+ " " + value.getClass());
            cat = (Category) value; 
            System.out.println(cat.getCategoryID()+ " " + cat.getCategoryName());
        }catch(Exception e){
                System.out.println("catc getAsString err "+ e);
        }
            //return ((Category) value).getKey();
        return Integer.toString(cat.getCategoryID());
    }
    
}
