package org.clibrary.db.jsf;

public class TwoStrings {
    public static class TwoStringsRecord {
		String string1;
		String string2;
	
		public TwoStringsRecord(String one, String two){
			this.string1 = one;
			this.string2 = two;
		}
		
		public String getOne(){
			return this.string1;
		}
		public String getTwo(){
			return this.string2;
		}
    }
}
