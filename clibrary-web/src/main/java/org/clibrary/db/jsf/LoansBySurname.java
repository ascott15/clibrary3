package org.clibrary.db.jsf;
import org.clibrary.db.jsf.TwoStrings.TwoStringsRecord;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LoansBySurname {	

	public List<TwoStringsRecord> list = new ArrayList<>();
	public List<TwoStringsRecord> getList(){
		GetRecords();
		return list;
	}
	public void GetRecords(){
		ResultSet resultset = null;
		Statement statement = null;
		Connection connection = null;
		try {
			connection = DBConn.getDBConn();
                            statement = connection.createStatement();
                            resultset = statement.executeQuery("select \"LastName\", count(*) from \"Loan\", \"Borrower\" " +
"                           where \"Loan\".\"BorrowerID\" = \"Borrower.\"BorrowerID\" and \"Loan\".\"Returned\" is null \n" +
"                           group by \"Borrower\".\"LastName\" order by 2;");
					this.list.clear();
                                        TwoStringsRecord loans;
					while(resultset.next()){
                                                loans = new TwoStringsRecord(resultset.getString(1), resultset.getString(2));
						this.list.add(loans); //the list hides details of the database from users
					}
		} catch (SQLException e) {
		} finally{
			try {
				if(connection != null)
					connection.close();
				if(resultset != null)
					resultset.close();
				if(statement != null)
					statement.close();
			} catch (SQLException e) {
			}
		}	
	}	
}
