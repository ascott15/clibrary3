package org.clibrary.db.jsf;

import org.clibrary.db.hibernate.Borrower;
import org.clibrary.db.jsf.util.JsfUtil;
import org.clibrary.db.jsf.util.JsfUtil.PersistAction;
import org.clibrary.db.bean.BorrowerFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("borrowerController")
@SessionScoped
public class BorrowerController implements Serializable {

    @EJB
    private BorrowerFacade ejbFacade;
    private List<Borrower> items = null;
    private Borrower selected;

    public BorrowerController() {
    }

    public Borrower getSelected() {
        return selected;
    }

    public void setSelected(Borrower selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private BorrowerFacade getFacade() {
        return ejbFacade;
//        return null;
    }

    public Borrower prepareCreate() {
        selected = new Borrower();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("BorrowerCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("BorrowerUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("BorrowerDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Borrower> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Borrower getBorrower(java.lang.String id) {
        return getFacade().find(id);
    }

    public List<Borrower> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Borrower> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Borrower.class)
    public static class BorrowerControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            BorrowerController controller = null;
            Object obj = null;
            try{
                if (value == null || value.length() == 0) {
                    return null;
                }
                controller = (BorrowerController) facesContext.getApplication().getELResolver().
                        getValue(facesContext.getELContext(), null, "borrowerController");
                obj = controller.getBorrower(getKey(value));
            } catch (Exception e){
                FacesMessage msg = new FacesMessage("borrowerController error: " + e);
                FacesContext.getCurrentInstance().addMessage(null, msg); 
            }
            return obj;
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Borrower) {
                Borrower o = (Borrower) object;
                return getStringKey(o.getBorrowerid());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Borrower.class.getName()});
                return null;
            }
        }

    }

}
