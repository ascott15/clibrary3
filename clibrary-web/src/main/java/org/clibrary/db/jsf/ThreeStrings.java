package org.clibrary.db.jsf;

public class ThreeStrings {
    public static class ThreeStringsRecord {
		String string1;
		String string2;
                String string3;
	
		public ThreeStringsRecord(String one, String two, String three){
			this.string1 = one;
			this.string2 = two;
                        this.string3 = three;
		}
		
		public String getOne(){
			return this.string1;
		}
		public String getTwo(){
			return this.string2;
		}
                public String getThree(){
			return this.string3;
		}
    }
}
