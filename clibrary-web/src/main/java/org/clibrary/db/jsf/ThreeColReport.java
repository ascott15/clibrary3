package org.clibrary.db.jsf;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.clibrary.db.jsf.ThreeStrings.ThreeStringsRecord;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import org.clibrary.db.jsf.util.JsfUtil;

public class ThreeColReport implements Serializable{
    FacesContext facesContext;
    Map<String, String> parametersPassed;
    String userChose;  
    List<ThreeStringsRecord> threeStringList = new ArrayList<>();
		
    
    private void getParameters(){
        try{
            facesContext = FacesContext.getCurrentInstance();
            parametersPassed = facesContext.getExternalContext().getRequestParameterMap();
            userChose = parametersPassed.get("choice");
            System.out.println("report getParameters "+ userChose);
        }catch (Exception e){
            System.out.println("report getParameters "+ e);
        }
    }
            
    public String getTitle(){
        getParameters();
        
        if(userChose==null){
            userChose = "9";
        }
        String title = "Title";
        switch (userChose) {
            case "1":
                title = "Last Time Borrowers Borrowed Report";
                break;
            case "2":
                title = "Borrowers Report by Month";
                break;
            case "3":
                title = "Loans by Surname Report";
                break;
            case "4":
                title = "Category Resources Report"; 
                break;
            case "5":
                title="Resource Type Report";
                break;
            case "6":
                title = "Borrowers Report by Year";
                break;
            case "7":
                title = "Resource's Loan History";
                break;    
            case "8":
                title = "All Current Loans"; 
                break;
            case "9":
                title = "Today's Returns";
                break;
            case "10":
                title = "Resources in the Library";  
                break;
            case "11":
                title = "Resources on loan";  
                break;
            case "12":
                title = "Resources lost";  
                break;    
        }
        return title;
    }
    public String getHeader1(){
        String header1Text = "Header";
        switch (userChose) {
            case "1":
                header1Text = "Name";
                break;
            case "2":
                header1Text = "Number of Borrowers";
                break;
            case "3":
                header1Text = "Surname";
                break;
            case "4":
                header1Text = "Category";  
                break;
            case "5":  
                header1Text = "Resource Type";
                break;
            case "6":
                header1Text = "Number of Borrowers"; 
                break;
            case "7":
                header1Text = "People"; 
                break;    
            case "8":
                header1Text = ""; 
                break;
            case "9":
                header1Text = "People";  
                break;
            case "10":
                header1Text = "Category";  
                break;
            case "11":
                header1Text = "Category";  
                break;
            case "12":
                header1Text = "Category";  
                break;    
        }
        return header1Text;
    }
    public String getHeader2(){
        String header2Text = "Header";
        switch (userChose) {
            case "1":
                header2Text = "Date";
                break;
            case "2":
                header2Text = "Month/Year";
                break;
            case "3":
                header2Text = "Loans";
                break;
            case "4":
                header2Text = "Number of Resources";  
                break;
            case "5":
                header2Text = "Number of Resources";
                break;
            case "6":
                header2Text = "Year"; 
                break;
             case "7":
                header2Text = "Loans"; 
                break;    
            case "8":
                header2Text = "";
                break;
             case "9":
                header2Text = "Resource";  
                break;
            case "10":
                header2Text = "Resource";
                break;
            case "11":
                header2Text = "Resource";
                break;
            case "12":
                header2Text = "Resource";
                break;    
        }
        return header2Text;
    }
    
    public String getHeader3(){
        String header2Text = "Header";
        switch (userChose) {
            case "1":
                header2Text = "Date";
                break;
            case "2":
                header2Text = "Month/Year";
                break;
            case "3":
                header2Text = "Loans";
                break;
            case "4":
                header2Text = "Number of Resources";  
                break;
            case "5":
                header2Text = "Number of Resources";
                break;
            case "6":
                header2Text = "Year"; 
                break;
             case "7":
                header2Text = "Loans"; 
                break;    
            case "8":
                header2Text = "";
                break;
             case "9":
                header2Text = "Resources";  
                break;
            case "10":
                header2Text = "Resource Type";
                break;
            case "11":
                header2Text = "Resource Type";
                break;
            case "12":
                header2Text = "Resource Type";
                break;    
        }
        return header2Text;
    }

    public List<ThreeStringsRecord> getList(){
        if(userChose == null){
            userChose = "10";
        }
        boolean valid = false;
        try{
            if(userChose.length() < 3){
                if(userChose.equals("7")|| userChose.equals("1")|| userChose.equals("2")|| userChose.equals("3")|| userChose.equals("4")|| userChose.equals("5")|| userChose.equals("6") || userChose.equals("8") || userChose.equals("9")|| userChose.equals("10")|| userChose.equals("11")|| userChose.equals("12") ){
                    //validate the number representing the choice of report entered by the user. The user could enter their own data here, a possible security concern.
                    valid=true;
                }
            }
        }catch(Exception e){
            System.out.println("report.java getList "+ e);
        }
        return GetRecords(valid);
    }
		
    public List<ThreeStringsRecord> GetRecords(boolean usersChoiceValid){
        ThreeStringsRecord threeStringsRecord = null;
        if (usersChoiceValid){
                    ResultSet resultset = null;
                    Statement statement = null;
                    PreparedStatement preparedStatement = null;
                    Connection connection = DBConn.getDBConn();
                    try{
			statement = connection.createStatement();
                        threeStringList.clear();                       
                        String SQLQuery = null;
                        switch (userChose) {
                            case "1":
                            //Last Time Each Borrower Borrowed Report
                            SQLQuery="Select concat_ws(', ', \"Borrower\".\"FirstName\"::text, "
                                + "\"Borrower\".\"LastName\"::text) as name, "
                                + "date_trunc('day'::text,max(\"Loan\".\"Borrowed\")) " +
                                "from \"Loan\", \"Borrower\" "+
                                "where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" "+
                                "group by \"Borrower\".\"FirstName\",\"Borrower\".\"LastName\" order by 1";
                            break;
                        case "2":
                            SQLQuery="select count(distinct \"Loan\".\"BorrowerID\"),"
                                + "date_part('Month',\"Loan\".\"Borrowed\"), date_part('year',\"Loan\".\"Borrowed\") "
                                + "from \"Loan\" "
                                +"group by date_part('Month',\"Loan\".\"Borrowed\"), "
                                + "date_part('year',\"Loan\".\"Borrowed\") order by 3, 2 desc";
                            break;             
                        /*case "3":
                            SQLQuery= "select \"Borrower\".\"LastName\", count(*) "
                                    + "from \"Loan\", \"Borrower\"" +
                                    " where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" "
                                    + "and \"Loan\".\"Returned\" is null " +
                                    " group by \"Borrower\".\"LastName\" order by 2 desc;";
                                //surnames of borrowers with resources out
                            break;
                        case "4":    
                            SQLQuery="SELECT \"Resource\".\"CategoryName\", count(*) "
                                + "FROM public.\"Resource\" WHERE \"Resource\".\"State\" = 'A' "
                                + "group by \"Resource\".\"CategoryName\" order by 2 desc";
                            break;
                        case "5":  
                            SQLQuery = "SELECT \"ResourceType\", count(*) FROM public.\"Resource\" "
                                + "WHERE \"Resource\".\"State\" = 'A' "
                                + "group by \"Resource\".\"ResourceType\" order by 2 desc";
                            break;
                        case "6":    
                            SQLQuery = "select count(distinct \"Loan\".\"BorrowerID\"),date_part('year',\"Loan\".\"Borrowed\") "
                                + "from public.\"Loan\" " +
                                "group by date_part('year',\"Loan\".\"Borrowed\") order by 2 desc"; 
                            break;
                        case "7":
                            //SQLQuery = "select \"Borrower\".\"FirstName\",\"Borrower\".\"LastName\", \"Loan\".\"Borrowed\" " +
                            //    "from public.\"Loan\", public.\"Resource\", public.\"Borrower\"\n" +
                            //    "WHERE public.\"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\"\n" +
                            //    "AND public.\"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\"\n" +
                            //    "AND upper(\"Resource\".\"ResourceID\") = ?";
                            
                            //connection.setAutoCommit(false);
                            //preparedStatement = connection.prepareStatement(SQLQuery);
                            //preparedStatement.setString(0, IDChosen);                          
                            //connection.commit();
                            break;*/
                        case "8":    
                            SQLQuery = "select \"Borrower\".\"FirstName\", \"Borrower\".\"LastName\", \"Resource\".\"ResourceName\", \"Loan\".\"Borrowed\" " +
                            "from \"Loan\",\"Borrower\",\"Resource\" " +
                            "Where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" " +
                            "AND \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" " +
                            "AND \"Loan\".\"Returned\" is null " +
                            "AND \"Resource\".\"State\" = 'A' " +        
                            "order by 1,2"; 
                            break;
                        case "9":
                            SQLQuery = "select \"Borrower\".\"FirstName\", \"Borrower\".\"LastName\", \"Resource\".\"ResourceName\" " +
                            "from public.\"Loan\",\"Borrower\",\"Resource\" "+
                            "Where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\" "+
                            "AND \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" "+ 
                            "AND \"Resource\".\"State\" = 'A' " +        
                            "AND age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) < '1 days'::interval " +
                            "ORDER by \"Loan\".\"Returned\",\"Borrower\".\"LastName\",\"Resource\".\"ResourceName\" "; //The last resource borrowed should be the first displayed.
                            break;
                        case "10":  
                            SQLQuery = "SELECT \"Category\".\"CategoryName\", \"Category\".\"Code\", \"Resource\".\"ResourceName\", \"Resource\".\"ResourceType\" " +
                            "FROM \"Resource\", \"Category\" " +
                            "WHERE \"Resource\".\"CategoryID\" = \"Category\".\"CategoryID\" " +
                            "AND \"Resource\".\"State\" = 'A' " +
                            "AND \"Resource\".\"ResourceID\" not in( " +
                            "	SELECT \"Loan\".\"ResourceID\" " +
                            "   FROM \"Loan\" " +
                            "	WHERE \"Loan\".\"Returned\" is null) ORDER BY 1,3";
                            break; //All resources currently in the library(not on loan or lost)
                        case "11":  
                            SQLQuery = "SELECT \"Category\".\"CategoryName\", \"Category\".\"Code\", \"Resource\".\"ResourceName\", \"Resource\".\"ResourceType\" \n" +
                            "FROM \"Resource\", \"Category\", \"Loan\" " +
                            "WHERE \"Resource\".\"CategoryID\" = \"Category\".\"CategoryID\" " +
                            "AND \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\" " +
                            "AND \"Loan\".\"Returned\" is null " +
                            "AND \"Resource\".\"State\" = 'A' " +
                            "ORDER BY \"Resource\".\"CategoryID\", \"Resource\".\"ResourceName\" ";
                            break; //Loaned resources
                        case "12":  
                            SQLQuery = "SELECT \"Category\".\"CategoryName\", \"Category\".\"Code\", \"Resource\".\"ResourceName\", \"Resource\".\"ResourceType\" " +
                            "FROM \"Resource\", \"Category\" " +
                            "WHERE \"Resource\".\"CategoryID\" = \"Category\".\"CategoryID\" " +
                            "AND \"Resource\".\"State\" <> 'A' " +
                            "ORDER BY \"Resource\".\"CategoryID\", \"Resource\".\"ResourceName\"";
                            System.out.println("rep "+ SQLQuery);
                            break;  //Lost Resources - can use Abstract facade FINDALLlost  
                        }  
                        if (userChose.equals("7")) {
                            resultset = preparedStatement.executeQuery();
                        } else {
                            resultset = statement.executeQuery(SQLQuery);
                        }
                        while(resultset.next()){
                            switch (userChose) {
                                case "8":
                                    threeStringsRecord = new ThreeStringsRecord(resultset.getString(1).concat(" ").concat(resultset.getString(2)),resultset.getString(3),resultset.getString(4));
                                    break;
                                case "10":
                                    threeStringsRecord = new ThreeStringsRecord(resultset.getString(1).concat(" ").concat(resultset.getString(2)),resultset.getString(3),resultset.getString(4));
                                    break;    
                                default:
                                    threeStringsRecord = new ThreeStringsRecord(resultset.getString(1),resultset.getString(2), resultset.getString(3));
                                    break;
                            }
                            this.threeStringList.add(threeStringsRecord); //the list hides details of the database from users
                        }
                    }catch(SQLException e1){
                        String msg = "";
                        Throwable cause = e1.getCause();
                        if (cause != null) {
                            msg = cause.getLocalizedMessage();
                        }
                        if (msg.length() > 0) {
                            JsfUtil.addErrorMessage(msg);
                        } else {
                            JsfUtil.addErrorMessage(e1, ResourceBundle.getBundle("/Bundle").getString("Error"));
                        }    
                    } finally {
                        try {
                            if(connection != null)
                                connection.close();
                            if(resultset != null)
                                resultset.close();
                            if(statement != null)
                                statement.close();
                        } catch(SQLException ex){
                            String msg = "";
                            Throwable cause = ex.getCause();
                            if (cause != null) {
                                msg = cause.getLocalizedMessage();
                            }
                            if (msg.length() > 0) {
                                JsfUtil.addErrorMessage(msg);
                            } else {
                                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                            }
                        }        
                    }
        } else {
            threeStringsRecord = new ThreeStringsRecord("Invalid choice,"," Please make a valid choice using the menu","");
            this.threeStringList.add(threeStringsRecord);
        }            
	return this.threeStringList;
    }           
            
    public boolean ValidateEnteredData(String data){
                boolean checkResult = false;
                String SQLQuery = "select max(length(\"Resource\".\"ResourceName\"))From public.\"Resource\" WHERE \"Resource\".\"State\" = 'A' ";
                int maxLength = 0;
                ResultSet resultset = null;
                Statement statement = null;
                
                Connection connection = DBConn.getDBConn();
                try{
                    statement = connection.createStatement();
                    resultset = statement.executeQuery(SQLQuery);
                        while(resultset.next()){   
                            maxLength = resultset.getInt(1);
                        }
                } catch(SQLException ex){
                        String msg = "";
                                Throwable cause = ex.getCause();
                                if (cause != null) {
                                    msg = cause.getLocalizedMessage();
                                }
                                if (msg.length() > 0) {
                                    JsfUtil.addErrorMessage(msg);
                                } else {
                                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                                }   
                } finally {
                        try {
                            if(connection != null)
                                connection.close();
                            if(resultset != null)
                                resultset.close();
                            if(statement != null)
                                statement.close();
                        } catch(SQLException ex){
                            String msg = "";
                                Throwable cause = ex.getCause();
                                if (cause != null) {
                                    msg = cause.getLocalizedMessage();
                                }
                                if (msg.length() > 0) {
                                    JsfUtil.addErrorMessage(msg);
                                } else {
                                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                                } 
                        }        
                }        
                //if(data.length() < 255){ //It should not exceed the maximum size of the Resource name field in the database
                if(data.length() <= maxLength){ //The entered search string should not exceed the maximum length of the longest Resource name in the database.
                    checkResult = true;
                }
        return checkResult;
    }    
}
