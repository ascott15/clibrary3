/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.db.jsf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author DL
 * adapted from: https://stackoverflow.com/questions/36993441/how-to-backup-a-postgres-database-using-java
 */

@Named(value = "backup")
@SessionScoped
public class Backup implements Serializable{
    String result;
    public String getResult() {
        try{
            result = Backup();
            if(result.equals("1")){
                result = "Unsuccessful";
            }   
        }catch (IOException | InterruptedException exception){
            System.out.println(exception);
        }
        return result;  
    }    
    public static String Backup() throws IOException, InterruptedException{
        BufferedReader bufferedReader;
        Map<String, String>environment;
        Process process = null;
        ProcessBuilder processBuilder = null;
        String readLine, returnValue = null;
        try{
            processBuilder = new ProcessBuilder(
            "C:\\Program Files\\PG\\9.6\\bin\\pg_dump.exe",
            "--host=localhost",
            "--port=5432",
            "--user=postgresql",        
            "--database=clibrary",        
            "--username=clibrary",
            "--password=clibrary",
            "--verbose",        
            "--file=C:\\Program Files\\PG\\9.6\\bin");  
            environment=processBuilder.environment();
            //environment.put("PGPASSWORD", "clibrary"); //insecure as users can see environment variables?

            System.out.println("env " + environment.entrySet());
            process = processBuilder.start();
            System.out.println("bup err" + process.getErrorStream().read() + " input" + process.getInputStream().read());
            bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            System.out.println("err stream " + process.getErrorStream().read() + " " + process.getInputStream().read());
            readLine = bufferedReader.readLine();
            System.out.println("bup readLine.length() = " +readLine.length());
            while(readLine!=null){ 
                readLine = bufferedReader.readLine();
                System.out.println("readLine = " + readLine);
            }
            bufferedReader.close();
            process.waitFor();
            System.out.println("fin result: " + process.exitValue());
            returnValue=Integer.toString(process.exitValue());
            System.out.println("ret=" + returnValue);
        } catch (IOException | InterruptedException exception){
            System.out.println(exception);
        } catch (Exception genException){
            System.out.println(genException);
        }  
        return returnValue;
    }
}
