package org.clibrary.db.jsf;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.clibrary.db.jsf.OverdueLoan.OverdueLoanRecord;
import org.clibrary.db.jsf.util.JsfUtil;
public class OverdueLoansReport implements Serializable{

	public List<OverdueLoanRecord> list = new ArrayList<>();
	public List<OverdueLoanRecord> getList(){
                try{
		GetRecords();
		} catch(NullPointerException e) {
			OverdueLoanRecord overdueLoanRecord = new OverdueLoanRecord("Database problem", "", "", "", "", "", "", "","","");
			this.list.add(overdueLoanRecord); //the list hides details of the database from users
		}
		return list;
	}
	public void GetRecords(){
		ResultSet resultset = null;
		Statement statement = null;
		Connection connection = null;
		try {
			connection = DBConn.getDBConn();
					statement = connection.createStatement();
                                        //A UNION command is used because different items have different loan periods
                                        //Includes Resources which may not have state = 'A'
					resultset = statement.executeQuery("SELECT \"Borrower\".\"FirstName\", \"Borrower\".\"LastName\",\n" +
                                            "\"Resource\".\"ResourceID\", \"Resource\".\"ResourceName\", \"Resource\".\"ResourceType\", \"Resource\".\"CategoryName\",\n" +
                                            "\"Loan\".\"Borrowed\"::timestamp::date,\n" +
                                            "date_trunc('day'::text, now() - \"Loan\".\"Borrowed\"::date::timestamp without time zone::timestamp with time zone) AS daysoverdue,\n" +
                                            "\"Borrower\".\"Phone\", \"Borrower\".\"Email\"\n" +
                                            "FROM public.\"Loan\", public.\"Borrower\", public.\"Resource\"\n" +
                                            "where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\"\n" +
                                            "and \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\"\n" +
                                            "and \"Loan\".\"Returned\" is null\n" +
                                            "and age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) > '14 days'::interval\n" +
                                            "AND (\"Resource\".\"ResourceType\" = 'CD' OR \"Resource\".\"ResourceType\" = 'DVD')\n" +
                                            "UNION\n" +
                                            "SELECT \"Borrower\".\"FirstName\", \"Borrower\".\"LastName\" ,\"Resource\".\"ResourceID\", \"Resource\".\"ResourceName\",\n" +
                                            "\"Resource\".\"ResourceType\", \"Resource\".\"CategoryName\",\n" +
                                            "\"Loan\".\"Borrowed\"::timestamp::date,\n" +
                                            "date_trunc('day'::text, now() - \"Loan\".\"Borrowed\"::date::timestamp without time zone::timestamp with time zone)\n" +
                                            "AS daysoverdue,\"Borrower\".\"Phone\", \"Borrower\".\"Email\"\n" +
                                            "FROM public.\"Loan\", public.\"Borrower\", public.\"Resource\"" +
                                            "where \"Loan\".\"BorrowerID\" = \"Borrower\".\"BorrowerID\"\n" +
                                            "and \"Loan\".\"ResourceID\" = \"Resource\".\"ResourceID\"\n" +
                                            "and \"Loan\".\"Returned\" is null\n" +
                                            "and age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) > '28 days'::interval\n" +
                                            "AND (\"Resource\".\"ResourceType\" != 'CD' AND \"Resource\".\"ResourceType\" != 'DVD')\n");
					this.list.clear();
					while(resultset.next()){
						OverdueLoanRecord overdueLoanRecord = new OverdueLoanRecord(resultset.getString(1), resultset.getString(2), resultset.getString(3), resultset.getString(4), resultset.getString(5), resultset.getString(6), resultset.getString(7), resultset.getString(8), resultset.getString(9), resultset.getString(10));
						this.list.add(overdueLoanRecord); //the list hides details of the database from users
					}
		} catch (SQLException e) {
                    String msg = "Overdue Loans Report Error: "+ e;
                    Throwable cause = e.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                    }
		} finally{
			try {
				if(connection != null)
					connection.close();
				if(resultset != null)
					resultset.close();
				if(statement != null)
					statement.close();
			} catch (SQLException e) {
                            String msg = "Overdue Loans Report Error: "+ e;
                            Throwable cause = e.getCause();
                            if (cause != null) {
                                msg = cause.getLocalizedMessage();
                            }
                            if (msg.length() > 0) {
                                JsfUtil.addErrorMessage(msg);
                            } else {
                                JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                            }
			}
		}	
	}	
}
