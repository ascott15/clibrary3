package org.clibrary.db.jsf;

public class OverdueLoan {

	public static class OverdueLoanRecord {
		String cname;
		String lname;
		String rid;
		String rname;
		String rtype;
		String catname;
		String db;
		String dover;
		String phone;
		String mail;
	
		OverdueLoanRecord(String cname, String lname, String rid, String rname, String rtype, String catname, String db, String dover, String phone, String mail){
			this.cname = cname;
			this.lname = lname;
			this.rid= rid;
			this.rname = rname;
			this.rtype = rtype;
			this.catname = catname;
			this.db = db;
			this.dover = dover;
			this.phone = phone;
			this.mail = mail;
		}
		
		public String getCname(){
			return this.cname;
		}
		public String getFamilyName(){
			return this.lname;
		}
		public String getRid(){
			return this.rid;
		}
		public String getRname(){
			return this.rname;
		}
		public String getRtype(){
			return this.rtype;
		}
		public String getCatName(){
			return this.catname;
		}
		public String getDb(){
			return this.db;
		}
		public String getDover(){
			return this.dover;
		}
		public String getPhone(){
			return this.phone;
		}
		public String getMail(){
			return this.mail;
		}
	}
}
