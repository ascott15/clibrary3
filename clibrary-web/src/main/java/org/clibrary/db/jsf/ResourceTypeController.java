package org.clibrary.db.jsf;

import org.clibrary.db.hibernate.Resourcetype;
import org.clibrary.db.jsf.util.JsfUtil;
import org.clibrary.db.jsf.util.JsfUtil.PersistAction;
import org.clibrary.db.bean.ResourceTypeFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("resourceTypeController")
@SessionScoped
public class ResourceTypeController implements Serializable {

    @EJB
    private ResourceTypeFacade ejbFacade;
    private List<Resourcetype> items = null;
    private Resourcetype selected;

    public ResourceTypeController() {
    }

    public Resourcetype getSelected() {
        return selected;
    }

    public void setSelected(Resourcetype selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ResourceTypeFacade getFacade() {
        return ejbFacade;
//        return null;
    }

    public Resourcetype prepareCreate() {
        selected = new Resourcetype();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ResourceTypeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ResourceTypeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ResourceTypeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Resourcetype> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Resourcetype getResourceType(java.lang.String id) {
        return getFacade().find(id);
    }

    public List<Resourcetype> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Resourcetype> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Resourcetype.class)
    public static class ResourceTypeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            ResourceTypeController controller = null;
            Object o = null;
            try{
                if (value == null || value.length() == 0) {
                    return null;
                }
                controller = (ResourceTypeController) facesContext.getApplication().getELResolver().
                        getValue(facesContext.getELContext(), null, "resourceTypeController");
                o = controller.getResourceType(getKey(value));
            } catch (NullPointerException npe){
                String msg = "";
                Throwable cause = npe.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(npe, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            }
            return o;
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Resourcetype) {
                Resourcetype o = (Resourcetype) object;
                return getStringKey(o.getResourcetype());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Resourcetype.class.getName()});
                return null;
            }
        }
    }
}