package org.clibrary.db.jsf;
import org.clibrary.db.jsf.TwoStrings.TwoStringsRecord;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class LoansReport {	

	public List<TwoStringsRecord> list = new ArrayList<>();
	public List<TwoStringsRecord> getList(){
		GetRecords();
		return list;
	}
	public void GetRecords(){
		ResultSet resultset = null;
		Statement statement = null;
		Connection connection = null;
		try {
                    connection = DBConn.getDBConn();
                        statement = connection.createStatement();
                        resultset = statement.executeQuery("select count(distinct \"Loan\".\"BorrowerID\"),"
                            + "date_part('Month',\"Loan\".\"Borrowed\"), date_part('year',\"Loan\".\"Borrowed\") "
                            + "from \"Loan\" "
                            + "group by date_part('Month',\"Loan\".\"Borrowed\"), date_part('year',\"Loan\".\"Borrowed\") "
                            + "order by 3, 2 desc");
                        this.list.clear();
                        TwoStringsRecord loans;
                        while(resultset.next()){
                            loans = new TwoStringsRecord(resultset.getString(1), resultset.getString(2).concat("/").concat(resultset.getString(3)));
                            this.list.add(loans); //the list hides details of the database from users
                        }                     
		} catch (SQLException e) {
                    FacesMessage msg = new FacesMessage("Loan report error: " + e);
                    FacesContext.getCurrentInstance().addMessage(null, msg); 
		} finally{
			try {
				if(connection != null)
					connection.close();
				if(resultset != null)
					resultset.close();
				if(statement != null)
					statement.close();
			} catch (SQLException e) {
                            FacesMessage msg = new FacesMessage("Loan report error: " + e);
                            FacesContext.getCurrentInstance().addMessage(null, msg); 
			}
		}	
	}	
}
