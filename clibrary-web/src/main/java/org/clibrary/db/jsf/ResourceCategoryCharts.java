package org.clibrary.db.jsf;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*/
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import org.clibrary.db.jsf.util.JsfUtil;
import org.primefaces.model.chart.LegendPlacement;
import org.primefaces.model.chart.PieChartModel;
/**
 *
 * @author DL
 */
@ManagedBean
@ViewScoped
public class ResourceCategoryCharts implements Serializable{
    PieChartModel pcm = new PieChartModel();
    private Map<String, Number> chartData = new LinkedHashMap<>();
    private static final long serialVersionUID =1L;
    
    String userChose;
    boolean userChoiceValid;

    @PostConstruct
    public void init(){
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> parametersPassed = fc.getExternalContext().getRequestParameterMap();
        userChose = parametersPassed.get("choice");
        userChoiceValid = ValidateUserChoice();
        
        setChartData(chartData); //why is this not called?
        pcm.setData(chartData);
        if(userChoiceValid){
            if(userChose.equals("1")){
                pcm.setTitle("Categories for Loans");
            } else if(userChose.equals("2")){
                pcm.setTitle("Categories for All Resources");
            }
        }
        pcm.setLegendPosition("e");
        pcm.setShowDataLabels(true);
        pcm.setLegendPlacement(LegendPlacement.OUTSIDE);
    }
    
    public boolean ValidateUserChoice(){
        boolean result = true;
        if(userChose.length() > 1){
            result = false;
        } else{
            if (!userChose.equals("1") && !userChose.equals("2")){
                System.out.println("user's choice <>1 or 2 choice="+ userChose);
                result = false;
            }
        }
        return result;
    }
    
    public PieChartModel getPcm(){
        return pcm;
    }
    public void setChartData(Map<String, Number> chartData){   
        String SQLQuery = null;
        if(userChoiceValid){
            if(userChose.equals("1")){
                SQLQuery = "SELECT \"ResourceType\", count(*) " +
                    "FROM public.\"Resource\", public.\"Loan\" "+
                    "WHERE \"Resource\".\"ResourceID\" = \"Loan\".\"ResourceID\" "+
                    "AND \"Resource\".\"State\" = 'A' "+
                    "and \"Loan\".\"Returned\" is null "+
                    "group by \"Resource\".\"ResourceType\" "+
                    "order by 2 desc ";
            } else{
                SQLQuery = "SELECT \"ResourceType\", count(*) "
                        + "FROM public.\"Resource\" WHERE \"Resource\".\"State\" = 'A' "
                        + "group by \"Resource\".\"ResourceType\" order by 2 desc";
            } 
        }
        ResultSet resultset = null;
        Statement statement = null;
        try{
        Connection connection = DBConn.getDBConn();
        statement = connection.createStatement();
        resultset = statement.executeQuery(SQLQuery);
            while(resultset.next()){
                chartData.put(resultset.getString(1), resultset.getInt(2));
            }            
        this.chartData = chartData;
        } catch(SQLException ex){
            String msg = "";
                    Throwable cause = ex.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
        }
    }
}