
package org.clibrary.db.jsf;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.clibrary.db.jsf.TwoStrings.TwoStringsRecord;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.clibrary.db.jsf.util.JsfUtil;

public class GenStatisticsReport implements Serializable{

    List<TwoStringsRecord> report = new ArrayList<>();
    static final long serialVersionUID = -1L;
	
    public List<TwoStringsRecord> getList(){
		return GetReport();
    }
	
    public List<TwoStringsRecord> GetReport() {
		ResultSet resultset = null;
		Statement statement = null;
		Connection connection = null;
                
                String avgLoanPeriod = "select AVG(age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) - age(\"Loan\".\"Returned\"::date::timestamp with time zone)) " +
                "from \"Loan\" where \"Loan\".\"Returned\" is not null";
                String mostLoanedCategory = "select \"Resource\".\"CategoryName\" from \"Resource\", \"Loan\" where \"Resource\".\"ResourceID\" = \"Loan\".\"ResourceID\" AND \"Resource\".\"State\" = 'A' group by \"Resource\".\"CategoryName\" order by count(\"Resource\".\"CategoryName\") desc limit 1";
                String mostLoanedResourceType = "select \"Resource\".\"ResourceType\" from \"Resource\", \"ResourceType\" where \"Resource\".\"ResourceID\" = \"ResourceType\".\"ResourceID\" AND \"Resource\".\"State\" = 'A' group by \"Resource\".\"ResourceTypeID\" order by count(\"Resource\".\"ResourceTypeID\") desc limit 1";
                String numBorrowersNotBorrowedMoreThanThreeMonths = "SELECT count(\"Loan\".\"BorrowerID\") "
		   + "FROM \"Loan\" "
		   + "where age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) > '3 mons'::interval "
		   + "and \"Loan\".\"BorrowerID\" not in( "
		   + "SELECT \"Loan\".\"BorrowerID\" "
		   + "FROM \"Loan\" "
		   + "where age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) < '3 mons'::interval)";
			// the 'not in' portion removes those who borrowed in the last three months
		String numBorrowersQuery = "SELECT count(*) FROM public.\"Borrower\"";
                String numBorrowersWhoBorrowedMoreThanThreeMonthsAgoWhoHaveNotReturned = "SELECT count(\"Loan\".\"BorrowerID\") FROM \"Loan\" "
				+ "where age (\"Loan\".\"Borrowed\"::date::timestamp with time zone) > "
						+ "'3 mons'::interval and \"Loan\".\"Returned\" is null";
                String numCurrentLoans = "SELECT count(*) FROM public.\"Loan\" where \"Loan\".\"Returned\" is null";
                String numLoansOverdueMoreThenOneYear = "Select count(*) from public.\"Loan\" where \"Loan\".\"Returned\" is null and age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) > '365 days'::interval";
                String numLoansWithBadResourceIDs ="select count(\"Loan\".\"ResourceID\") from \"Loan\" where substr(\"Loan\".\"ResourceID\",1,1) != 'R'";
                String numOverdueLoans = "SELECT count(*) " 
				+ " FROM public.\"Loan\""
				+ " where \"Loan\".\"Returned\" is null"
				+ " and age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) > '14 days'::interval ";
		String numResourcesLoanedLast30Days = "select count(*) from \"Loan\" where \"Loan\".\"Returned\" is null and age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) < '30 days'::interval";
                String numResourcesNeverBorrowed = "select count(*) from \"Resource\" where \"Resource\".\"ResourceID\" not in(select \"Loan\".\"ResourceID\" from \"Loan\")";
                String numResourcesQuery = "SELECT count(*) FROM public.\"Resource\" WHERE \"Resource\".\"State\" = 'A'";
                String numBorrowersLast30Days = "select count(distinct \"Loan\".\"BorrowerID\") from \"Loan\" where \"Loan\".\"Returned\" is null and age(\"Loan\".\"Borrowed\"::date::timestamp with time zone) < '30 days'::interval";
                String numberPeopleWithMoreThan4Resources = "select count(distinct \"Loan\".\"BorrowerID\") from \"Loan\" where \"Loan\".\"Returned\" is null "
                 +"group by \"Loan\".\"BorrowerID\" having count(\"Loan\".\"ResourceID\") > 4 order by 1";
                //to_char(loan."Borrowed", 'Month YYYY') postgresql format of date into, eg March
                String numResourcesWithBadResourceIDs ="select count(\"Resource\".\"ResourceID\") from \"Resource\" where substr(\"Resource\".\"ResourceID\",1,1) != 'R'";
                String numUnavailableResources = "Select count(*) from \"Resource\" where \"Resource\".\"State\" <> 'A'";
                
                report.clear();
		try{
					connection = DBConn.getDBConn();
					statement = connection.createStatement();
                                        TwoStringsRecord twoStringsRecord = null;
                            
                                        resultset = statement.executeQuery(numberPeopleWithMoreThan4Resources);
                                        if(resultset.isBeforeFirst()){
                                            while(resultset.next()){
                                                twoStringsRecord = new TwoStringsRecord("Number of people with more than 4 resources", resultset.getString(1));
                                            }
					this.report.add(twoStringsRecord);   //if there are no records then this rep[ort is not shown
                                        }
                                        //Does the having result count loans where returned is null?
                                 
					resultset = statement.executeQuery(numBorrowersQuery);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of borrowers", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);  
					
                                        resultset.close();
					resultset = statement.executeQuery(numBorrowersLast30Days);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of borrowers active over the last 30 days", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);  
                                  
                                        resultset = statement.executeQuery(numBorrowersNotBorrowedMoreThanThreeMonths);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of borrowers who have not borrowed for more than 3 months", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);  
					
					resultset = statement.executeQuery(numBorrowersWhoBorrowedMoreThanThreeMonthsAgoWhoHaveNotReturned);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of borrowers who have not returned resources borrowed more than 3 months ago", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);  
                                        
                                        resultset = statement.executeQuery(avgLoanPeriod);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Average loan period(only includes returned resources)", resultset.getString(1));
					}
                                        //truncate result to months & days only
                                        
					this.report.add(twoStringsRecord);   //if there are many loans of long duration the result will be skewed
                                        
                                        
					resultset = statement.executeQuery(numCurrentLoans);
                                        int numCurrentLoansInt=0;
					while(resultset.next()){
                                                numCurrentLoansInt = resultset.getInt(1);
                                                twoStringsRecord = new TwoStringsRecord("Number of current loans", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);  
                                        
					resultset = statement.executeQuery(numOverdueLoans);
                                        int numOverdueLoansInt = 0;
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of overdue loans ", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);  
                           
					resultset = statement.executeQuery(numLoansOverdueMoreThenOneYear);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of loans overdue by more than one year", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);  
                                        
                                        resultset = statement.executeQuery(numLoansWithBadResourceIDs);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of loans with resource IDs that do not begin with 'R'", resultset.getString(1));	
					}
					this.report.add(twoStringsRecord);  
                                        
                                        resultset = statement.executeQuery(numResourcesWithBadResourceIDs);
                                        numResourcesWithBadResourceIDs = "Number of resources with resource IDs that do not begin with 'R'";
					while(resultset.next()){
                                                twoStringsRecord = new TwoStringsRecord(numResourcesWithBadResourceIDs, resultset.getString(1));
					}
					this.report.add(twoStringsRecord);
                                        
                                        resultset.close();
					resultset = statement.executeQuery(numResourcesQuery);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of Available Resources", resultset.getString(1));	
					}
					this.report.add(twoStringsRecord); 
                                        
                                        resultset = statement.executeQuery(numUnavailableResources);
                                        numUnavailableResources = "Number of unavailable resources(state is not set to 'A') ";
					while(resultset.next()){
                                                twoStringsRecord = new TwoStringsRecord(numUnavailableResources, resultset.getString(1));
					}
                                        this.report.add(twoStringsRecord);
                       
                                        int percentCurrentLoansOverdue = (numOverdueLoansInt/numCurrentLoansInt)*100;
                                        twoStringsRecord = new TwoStringsRecord("Percent of resources out which are overdue ", percentCurrentLoansOverdue + "%");
					this.report.add(twoStringsRecord);  
				
                                        resultset = statement.executeQuery(numResourcesNeverBorrowed);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of resources never borrowed", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);  
                                        
                                        resultset = statement.executeQuery(numResourcesLoanedLast30Days);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Number of resources loaned over the last 30 days", resultset.getString(1));
					}
					this.report.add(twoStringsRecord);
                                        
                                        resultset = statement.executeQuery(mostLoanedCategory);
					while(resultset.next()){
                                            twoStringsRecord = new TwoStringsRecord("Most borrowed category", resultset.getString(1));	
					}
					this.report.add(twoStringsRecord);  
		} catch (SQLException ex) {
                    String msg = "";
                    Throwable cause = ex.getCause();
                    if (cause != null) {
                        msg = cause.getLocalizedMessage();
                    }
                    if (msg.length() > 0) {
                        JsfUtil.addErrorMessage(msg);
                    } else {
                        JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                    }
		} finally{
			try {
				if(connection != null)
					connection.close();
				if(resultset != null)
					resultset.close();
				if(statement != null)
					statement.close();
			} catch (SQLException ex) {
                            String msg = "";
                            Throwable cause = ex.getCause();
                            if (cause != null) {
                                msg = cause.getLocalizedMessage();
                            }
                            if (msg.length() > 0) {
                                JsfUtil.addErrorMessage(msg);
                            } else {
                                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("Error"));
                            }
			}
		}	
        return report;
	}	
}
