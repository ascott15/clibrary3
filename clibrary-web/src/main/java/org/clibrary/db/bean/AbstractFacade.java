/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.db.bean;

import java.math.BigInteger;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author ascott
 * @param <T>
 */
public abstract class AbstractFacade<T> {

    private final Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     *
     * @return How do you test this works?
     */
    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        try{
            if(getEntityManager().contains(entity)){
                System.out.println("af cx merge " + entity.toString() + " " + getEntityManager().toString());
                getEntityManager().merge(entity);
            } else{
                System.out.println("af cx persist " + entity.toString() + " " + getEntityManager().toString());
                getEntityManager().persist(entity); //may cause: javax.persistence.PersistenceException: org.hibernate.PersistentObjectException: detached entity passed to persist: org.clibrary.db.hibernate.Borrower
            }
        } catch (Exception ex){
                System.out.println("AF Could not create the new " + entity.getClass() + ": " + ex);
                FacesMessage msg = new FacesMessage("AF Could not merge: " + ex);
                FacesContext.getCurrentInstance().addMessage(null, msg);
        }            
    }

    public void edit(T entity) {
        T a = getEntityManager().merge(entity);
        System.out.println("af post edit " +a.getClass() + " entity = "+ entity.getClass());
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        System.out.println("af find class " + getEntityManager().find(entityClass, id).getClass() + " id=" + id);
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        //On a Windows PC: C:\Users\DL\workspace\clibrary3-db\sc\main\java\org\clibrary\db\hibernatequeries
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        //System.out.println("Abstract facade FINDALL called " + entityClass.toString() + " " + cq.toString());
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findAllLost() {
        //This must call a different query to findAll()
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        System.out.println("Abstract facade FINDALLlost called " + entityClass.toString() + " " + cq.toString());
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }
    
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        //q.setMaxResults(range[1] - range[0] + 1);
        q.setMaxResults(4);
        //for (int i=0; i < range.length; i++){
            q.setFirstResult(range[0]);
        //}
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Number) q.getSingleResult()).intValue();
    }
    
    private static final String SQL_NEXT_ID = "select nextval('###ID_SEQ###')";
    public long nextId() {
//        Query query = getEntityManager().createNamedQuery(SQL_NEXT_ID);
//        query.setParameter(1, query);

//    getEntityManager().createNativeQuery(SQL_NEXT_ID, SQL_NEXT_ID)
            
        String sql = SQL_NEXT_ID.replace("###ID_SEQ###", entityClass.getSimpleName().toLowerCase() + "idgen");
        Query query = getEntityManager().createNativeQuery(sql);
//        List<Long> longList = query.getResultList();
        BigInteger id = (BigInteger) query.getSingleResult();
        
        return id.longValue();
    }
}
