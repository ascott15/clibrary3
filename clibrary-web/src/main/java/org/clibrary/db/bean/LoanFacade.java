/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.clibrary.db.bean;

import org.clibrary.db.hibernate.Loan;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ascott
 */
@Stateless
public class LoanFacade extends AbstractFacade<Loan> {

    @PersistenceContext(unitName = "au.net.ascott_clibrary-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
       return em;
    }

    public LoanFacade() {
        super(Loan.class);
    }
    
}
