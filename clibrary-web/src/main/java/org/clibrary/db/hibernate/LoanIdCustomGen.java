package org.clibrary.db.hibernate;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.type.Type;

public class LoanIdCustomGen implements IdentifierGenerator, Configurable {

	/**
	 * A borrower ID is prefixed with 'B' followed by a minimum of 5 digits.
	 */
		private String getSequenceSql = null;

	@Override
	public void configure(Type type, Properties params, Dialect dialect) throws MappingException {

		getSequenceSql = dialect.getSequenceNextValString("LoanIdGen");
	}

	@Override
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {

		try {
			Connection connection = session.connection();
			try (PreparedStatement preparedStmt = connection.prepareStatement(getSequenceSql)) {
				ResultSet resultSet = preparedStmt.executeQuery();
				resultSet.next();
				Long value = resultSet.getLong(1);
				return value;
			}
		} catch (SQLException e) {
			throw new HibernateException("Unable to get LoanIdGen sequence value", e);
		}
	}

//	@Override
//	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
//
//		BigInteger bigInt = (BigInteger) session.createNativeQuery(getSequenceSql).getSingleResult();
//		String id = idFormat.format(bigInt.longValue());
//		return id;
//	}
//
//	@Override
//	public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
//
//		System.out.println("configure executed.");
//
//		JdbcEnvironment jdbcEnv = serviceRegistry.getService(JdbcEnvironment.class);
//		Dialect dialect = jdbcEnv.getDialect();
//		getSequenceSql = dialect.getSequenceNextValString("BorrowerIdGen");
//	}

}