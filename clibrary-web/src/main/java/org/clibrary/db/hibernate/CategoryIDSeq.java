package org.clibrary.db.hibernate;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.type.Type;

public class CategoryIDSeq implements IdentifierGenerator, Configurable {
		private String getSequenceSql = null;

	@Override
	public void configure(Type type, Properties params, Dialect dialect) throws MappingException {
            String a = "\"Category_CategoryID_seq\"";
	    getSequenceSql = dialect.getSequenceNextValString(a);
	}

	@Override
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
            try {
                Connection connection = session.connection();
                try (PreparedStatement preparedStmt = connection.prepareStatement(getSequenceSql)) {
                    ResultSet resultSet = preparedStmt.executeQuery();
                    resultSet.next();
                    Long value = resultSet.getLong(1);
                    return value;
                }
            } catch (SQLException e) {
                    throw new HibernateException("Unable to get CategoryId sequence value", e);
            }
	}
}        