package org.clibrary.db.hibernate;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.type.Type;

public class ResourceIdCustomGen implements IdentifierGenerator, Configurable {

	/**
	 * A resource ID is prefixed with 'R' followed by a minimum of 5 digits.
	 */
	private final DecimalFormat idFormat = new DecimalFormat("R##########00000");

	private String getSequenceSql = null;

	@Override
	public void configure(Type type, Properties params, Dialect dialect) throws MappingException {
		getSequenceSql = dialect.getSequenceNextValString("ResourceIdGen");
	}

	@Override
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
		try {
                    System.out.println("custom gen generate " + getSequenceSql);
			Connection connection = session.connection();
                        System.out.println("post conn");
			try (PreparedStatement preparedStmt = connection.prepareStatement(getSequenceSql)) {
				ResultSet resultSet = preparedStmt.executeQuery();
				resultSet.next();
				Long value = resultSet.getLong(1);
				String id = idFormat.format(value);
				return id;
			}
		} catch (SQLException e) {
			throw new HibernateException("Unable to get ResourceIdGen sequence value", e);
		}
	}
}